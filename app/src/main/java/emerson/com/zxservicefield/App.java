package emerson.com.zxservicefield;

import android.app.Application;

import org.greenrobot.eventbus.EventBus;

import emerson.com.zxservicefield.data.component.DaggerNetComponent;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.data.module.NetModule;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.data.source.local.DBHelper;

/**
 * Created by mobileapps on 10/24/16.
 */

public class App extends Application {

    private String rootUrl = "http://partnerplus.emerson.com/";
    private NetComponent mNetComponent;
    private ProfileModel profileModel;
    private DBHelper dbHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(rootUrl))
                .build();


    }

    public DBHelper getDbHandler() {
        return dbHandler;
    }

    public ProfileModel getProfileModel() {
        return profileModel;
    }

    public void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

    public void createDB(EventBus eventBus) {
        dbHandler = new DBHelper(getApplicationContext());
        if(dbHandler != null)
        {
            dbHandler.close();
            dbHandler.createDB(eventBus);
        }
    }
}
