package emerson.com.zxservicefield.home.qr.application;

/**
 * Created by mobileapps on 11/10/16.
 */

public interface ApplicationContract {

    interface View{
        void onSuccess();
        void onFailed();
    }

    interface Presenter{
        void setApplicationType(String modelNum,String serialNumber, String applicationTypeID);
    }

}
