package emerson.com.zxservicefield.home.qr.verification;

import dagger.Module;
import dagger.Provides;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/2/16.
 */

@Module
public class VerificationModule {
    private final VerificationContract.View mView;


    public VerificationModule(VerificationContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    VerificationContract.View providesVerificationContractView() {
        return mView;
    }
}