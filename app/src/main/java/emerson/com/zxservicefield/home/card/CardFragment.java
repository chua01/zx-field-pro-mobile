package emerson.com.zxservicefield.home.card;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import butterknife.Bind;
import butterknife.ButterKnife;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.HomeToolbar;

;

/**
 * Created by jeanang on 5/27/16.
 */
public class CardFragment extends Fragment implements HomeToolbar {

    @Bind(R.id.tv_member_id)
    TextView tvMemberId;
    @Bind(R.id.tv_name)
    TextView tvName;
    @Bind(R.id.img_card)
    ImageView imgCard;

    private View mView;
    private App mApp;
    private HomeActivity mHomeActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_card, container, false);
        ButterKnife.bind(this, mView);

        mApp = (App) getActivity().getApplication();

        tvMemberId.setText(mApp.getProfileModel().getID());
        tvName.setText(mApp.getProfileModel().getC_FirstName() + " " + mApp.getProfileModel().getC_LastName());

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext())
                // You can pass your own memory cache implementation
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .build();

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(50)) //rounded corner bitmap
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        String img = "drawable://" + R.drawable.ic_membership_card;
        imageLoader.displayImage(img, imgCard, options);

        mHomeActivity = ((HomeActivity)getActivity());
        mHomeActivity.setToolbar(this);
        mHomeActivity.setColorBackground(R.color.color_card);
        mHomeActivity.setLeftButtonVisibility(View.GONE);
        mHomeActivity.setRightButtonVisibility(View.GONE);
        mHomeActivity.setTitle(getResources().getString(R.string.title_card));

        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onLeftButtonClicked() {

    }

    @Override
    public void onRightButtonClicked() {

    }
}
