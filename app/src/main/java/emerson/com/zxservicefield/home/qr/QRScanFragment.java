package emerson.com.zxservicefield.home.qr;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.HomeToolbar;
import emerson.com.zxservicefield.home.qr.input.InputFragment;
import emerson.com.zxservicefield.home.qr.scan.ScanFragment;

/**
 * Created by mobileapps on 10/28/16.
 */

public class QRScanFragment extends Fragment implements HomeToolbar{

    @Bind(R.id.scanLayout)
    LinearLayout mScanLayout;
    @Bind(R.id.inputLayout)
    LinearLayout mInputLayout;

    private HomeActivity mHomeActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scanqr,container,false);
        ButterKnife.bind(this,view);
        mHomeActivity = ((HomeActivity)getActivity());
        mHomeActivity.setToolbar(this);
        mHomeActivity.setTabBarVisibility(View.VISIBLE);
        mHomeActivity.setLeftButtonVisibility(View.GONE);
        mHomeActivity.setRightButtonVisibility(View.GONE);
        mHomeActivity.setTitle(getResources().getString(R.string.title_unit_registration));
        mHomeActivity.setColorBackground(getResources().getColor(R.color.color_unit_registration));


        return view;
    }

    @OnClick({R.id.scanLayout,R.id.inputLayout})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.scanLayout:
                ScanFragment scanFragment = new ScanFragment();
                pushFragment(scanFragment,getResources().getString(R.string.tag_scan));
                break;
            case R.id.inputLayout:
                InputFragment inputFragment = new InputFragment();
                pushFragment(inputFragment,getResources().getString(R.string.tag_input));
                break;
        }

    }


    @Override
    public void onLeftButtonClicked() {

    }

    @Override
    public void onRightButtonClicked() {

    }

    private void pushFragment(Fragment fragment, String tag){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout,fragment,tag);
        ft.addToBackStack(tag);
        ft.commit();
    }
}

