package emerson.com.zxservicefield.home.qr.verification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.UnitModel;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.qr.registered.RegisteredFragment;

/**
 * Created by mobileapps on 11/4/16.
 */

public class VerificationFragment extends Fragment implements VerificationContract.View {

    private final String TAG = VerificationFragment.class.getSimpleName();

    private HomeActivity mHomeActivity;
    private UnitModel mUnitModel;

    @Inject
    VerificationPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verification, container, false);
        mHomeActivity = ((HomeActivity) getActivity());
        mHomeActivity.setTabBarVisibility(View.GONE);

        DaggerVerificationComponent.builder()
                .netComponent(((App) getActivity().getApplication()).getNetComponent())
                .verificationModule(new VerificationModule(this))
                .build().inject(this);

        mPresenter.verify(getActivity(), mUnitModel);

        return view;
    }

    public void setUnitModel(UnitModel mUnitModel) {
        this.mUnitModel = mUnitModel;
    }

    @Override
    public void onDestroyView() {
        mHomeActivity.setTabBarVisibility(View.VISIBLE);
        super.onDestroyView();
    }

    @Override
    public void onSuccess(UnitModel unitModel) {
//        if (unitModel.getStatus().equals("1.0")) {
//            Log.d(TAG, "registered");
//            RegisteredFragment successFragment = new RegisteredFragment();
//            pushFragment(successFragment, "success");
//        } else {
//            Log.d(TAG, "already registered");
//            AlreadyRegisteredFragment alreadyRegisteredFragment = new AlreadyRegisteredFragment();
//            pushFragment(alreadyRegisteredFragment, "alreadyregistered");
//
//        }
        Log.d(TAG, "registered");
        RegisteredFragment successFragment = new RegisteredFragment();
        successFragment.setUnitModel(unitModel);
        pushFragment(successFragment, "success");
    }

    @Override
    public void onFailed() {

    }

    private void pushFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment, tag);

        ft.commit();

    }

}
