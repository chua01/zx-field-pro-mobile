package emerson.com.zxservicefield.home.qr.step;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.util.AppUtils;

/**
 * Created by mobileapps on 11/15/16.
 */

public class ImageViewer extends Fragment implements OnTouchListener {

    private String mLink;
    private AppUtils mAppUtils;

    @Bind(R.id.imageEnhance)
    ImageView mImageView;

    // These matrices will be used to move and zoom image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    String savedItemClicked;
    private ScaleGestureDetector scaleGestureDetector;

    public void setLink(String mLink) {
        this.mLink = mLink;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_viewer,container,false);
        ButterKnife.bind(this,view);

        mAppUtils = new AppUtils(getActivity());

        mAppUtils.cacheImage(mImageView, mLink);

        scaleGestureDetector = new ScaleGestureDetector(
                getActivity(), new MySimpleOnScaleGestureListener(mImageView));

        mImageView.setOnTouchListener(this);
        return view;
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        scaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class MySimpleOnScaleGestureListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener{

        ImageView viewMyImage;

        float factor;

        public MySimpleOnScaleGestureListener(ImageView iv) {
            super();
            viewMyImage = iv;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            factor = 1.0f;
            return true;
            //return super.onScaleBegin(detector);
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            float scaleFactor = detector.getScaleFactor() - 1;
            factor += scaleFactor;
            viewMyImage.setScaleX(factor);
            viewMyImage.setScaleY(factor);
            return true;
            //return super.onScale(detector);
        }
    }
}
