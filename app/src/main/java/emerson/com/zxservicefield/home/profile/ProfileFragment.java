package emerson.com.zxservicefield.home.profile;

/**
 * Created by mobileapps on 10/27/16.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.download.ImageDownloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.Preferences;
import emerson.com.zxservicefield.data.response.Countries;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.HomeToolbar;
import emerson.com.zxservicefield.login.LoginActivity;
import emerson.com.zxservicefield.util.AppUtils;

/**
 * Created by jeanang on 5/27/16.
 */

public class ProfileFragment extends Fragment implements ProfileContract.View, HomeToolbar, RadioGroup.OnCheckedChangeListener {

    @Bind(R.id.rel_profile_1)
    RelativeLayout relProfile1;
    @Bind(R.id.rel_profile_2)
    RelativeLayout relProfile2;
    @Bind(R.id.rel_profile_3)
    RelativeLayout relProfile3;
    @Bind(R.id.rel_profile_4)
    RelativeLayout relProfile4;
    @Bind(R.id.rel_profile_5)
    RelativeLayout relProfile5;
    @Bind(R.id.rel_profile_6)
    RelativeLayout relProfile6;
    @Bind(R.id.rel_profile_7)
    RelativeLayout relProfile7;
    @Bind(R.id.rel_profile_8)
    RelativeLayout relProfile8;
    @Bind(R.id.rel_profile_9)
    RelativeLayout relProfile9;
    @Bind(R.id.rel_profile_10)
    RelativeLayout relProfile10;
    @Bind(R.id.img_profile_picture)
    ImageView imgProfilePicture;
    @Bind(R.id.et_last_name)
    EditText etLastName;
    @Bind(R.id.et_first_name)
    EditText etFirstName;
    @Bind(R.id.et_company_name)
    EditText etCompanyName;
    @Bind(R.id.et_business_address)
    EditText etBusinessAddress;
    @Bind(R.id.tv_country)
    TextView tvCountry;
    @Bind(R.id.tv_dial_code)
    TextView tvDialCode;
    @Bind(R.id.et_mobile)
    EditText etMobile;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.lin_profile_holder)
    LinearLayout linProfileHolder;
    @Bind(R.id.progress_bar)
    NumberProgressBar progressBar;
    @Bind(R.id.tv_city)
    TextView tvCity;
    @Bind(R.id.et_job_title)
    TextView etJobTitle;
    @Bind(R.id.radio_group_2)
    RadioGroup radioGroup2;
    @Bind(R.id.radio_group_3)
    RadioGroup radioGroup3;
    @Bind(R.id.radio_group_5)
    RadioGroup radioGroup5;
    @Bind(R.id.radio_group_6)
    RadioGroup radioGroup6;
    @Bind(R.id.radio_group_80)
    RadioGroup radioGroup80;
    @Bind(R.id.radio_group_81)
    RadioGroup radioGroup81;
    @Bind(R.id.radio_group_90)
    RadioGroup radioGroup90;
    @Bind(R.id.radio_group_91)
    RadioGroup radioGroup91;
    @Bind(R.id.chk_40)
    CheckBox chk40;
    @Bind(R.id.chk_41)
    CheckBox chk41;
    @Bind(R.id.chk_42)
    CheckBox chk42;
    @Bind(R.id.chk_43)
    CheckBox chk43;
    @Bind(R.id.chk_44)
    CheckBox chk44;
    @Bind(R.id.chk_45)
    CheckBox chk45;
    @Bind(R.id.chk_46)
    CheckBox chk46;
    @Bind(R.id.chk_47)
    CheckBox chk47;
    @Bind(R.id.chk_48)
    CheckBox chk48;
    @Bind(R.id.chk_49)
    CheckBox chk49;
    @Bind(R.id.chk_410)
    CheckBox chk410;
    @Bind(R.id.chk_70)
    CheckBox chk70;
    @Bind(R.id.chk_71)
    CheckBox chk71;
    @Bind(R.id.chk_72)
    CheckBox chk72;
    @Bind(R.id.chk_73)
    CheckBox chk73;
    @Bind(R.id.chk_74)
    CheckBox chk74;
    @Bind(R.id.chk_75)
    CheckBox chk75;
    @Bind(R.id.chk_100)
    CheckBox chk100;
    @Bind(R.id.chk_101)
    CheckBox chk101;
    @Bind(R.id.chk_102)
    CheckBox chk102;
    @Bind(R.id.chk_103)
    CheckBox chk103;
    @Bind(R.id.chk_104)
    CheckBox chk104;
    @Bind(R.id.chk_105)
    CheckBox chk105;
    @Bind(R.id.chk_106)
    CheckBox chk106;
    @Bind(R.id.chk_107)
    CheckBox chk107;
    @Bind(R.id.chk_108)
    CheckBox chk108;
    @Bind(R.id.chk_109)
    CheckBox chk109;
    @Bind(R.id.tv_question_4)
    TextView tvQuestion4;
    @Bind(R.id.profile_disclaimer)
    TextView profileDisclaimer;

    private final String TAG = ProfileFragment.class.getSimpleName();
    private final String INTERNAL = "Internal";


    private View view;
    private ArrayList<Countries> countries;
    private ArrayList<String> cities;
    private App app;
    private ProfileModel profileModel;
    private ArrayList<String> list_selection_3, list_selection_6, list_selection_11;
    private ArrayList<CheckBox> list_checkbox_3, list_checkbox_6, list_checkbox_11;
    private int tag = 1;
    private int max_screens = 0;
    private SweetAlertDialog dialog;
    private AlertDialog dialog_2 = null;

    private int[] checkedID;
    private AppUtils mUtils;
    private SharedPreferences mSharedPreferences;
    private HomeActivity mHomeActivity;

    @Inject
    ProfilePresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);


        mHomeActivity = ((HomeActivity) getActivity());
        mHomeActivity.setToolbar(this);
        mHomeActivity.setLeftButtonVisibility(View.VISIBLE);
        mHomeActivity.setRightButtonVisibility(View.VISIBLE);
        mHomeActivity.setLeftButton(R.drawable.ic_logout);

        mHomeActivity.setTitle(getResources().getString(R.string.title_profile));
        mHomeActivity.setColorBackground(getResources().getColor(R.color.colorPrimary));

        DaggerProfileComponent.builder()
                .netComponent(((App) getActivity().getApplicationContext()).getNetComponent())
                .profileModule(new ProfileModule(this))
                .build().inject(this);

        app = (App) getActivity().getApplication();

        mUtils = new AppUtils(getActivity());
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        profileModel = app.getProfileModel();

        fillArrayCheckBox();

        list_selection_3 = new ArrayList<>();
        list_selection_6 = new ArrayList<>();
        list_selection_11 = new ArrayList<>();

        radioGroup2.setOnCheckedChangeListener(this);
        radioGroup3.setOnCheckedChangeListener(this);
        radioGroup5.setOnCheckedChangeListener(this);
        radioGroup6.setOnCheckedChangeListener(this);
        radioGroup80.setOnCheckedChangeListener(this);
        radioGroup81.setOnCheckedChangeListener(this);
        radioGroup90.setOnCheckedChangeListener(this);
        radioGroup91.setOnCheckedChangeListener(this);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/times.ttf");
        profileDisclaimer.setTypeface(tf);

        changeLayout();

        String picturePath = mSharedPreferences.getString(Preferences.PROFILE_PICTUREPATH, "");
        if (!TextUtils.isEmpty(picturePath)) {
            mUtils.cacheImage(imgProfilePicture, picturePath);
        }

        setupData();


        return view;
    }


    public void getCountries() {
        final String[] items = new String[countries.size()];
        final int[] selected = {-1};

        for (int i = 0; i < countries.size(); i++) {
            items[i] = countries.get(i).getName();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.dialog_select_country));
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pos) {
                selected[0] = pos;
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
            }
        });

        builder.setPositiveButton(getResources().getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvCountry.setText(items[selected[0]]);
                tvDialCode.setText(countries.get(selected[0]).getDial_code());
            }
        });

        builder.setNeutralButton(getResources().getString(R.string.dialog_dismiss), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog_2.dismiss();
            }
        });

        dialog_2 = builder.create();

        dialog_2.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnPositive = dialog_2.getButton(AlertDialog.BUTTON_POSITIVE);
                Button btnNegative = dialog_2.getButton(AlertDialog.BUTTON_NEUTRAL);

                btnPositive.setEnabled(false);
            }
        });

        dialog_2.show();
    }

    public void getCities() {
        final String[] items = new String[cities.size()];
        final int[] selected = {-1};

        for (int i = 0; i < cities.size(); i++) {
            items[i] = cities.get(i);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.dialog_select_city));
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pos) {
                selected[0] = pos;
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
            }
        });

        builder.setPositiveButton(getResources().getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvCity.setText(items[selected[0]]);
            }
        });

        builder.setNeutralButton(getResources().getString(R.string.dialog_dismiss), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog_2.dismiss();
            }
        });

        dialog_2 = builder.create();

        dialog_2.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnPositive = dialog_2.getButton(AlertDialog.BUTTON_POSITIVE);
                Button btnNegative = dialog_2.getButton(AlertDialog.BUTTON_NEUTRAL);

                btnPositive.setEnabled(false);
            }
        });

        dialog_2.show();
    }

    private void setCheckedRadioButtonFromRadioGroup(RadioGroup radioGroup, String text) {
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            RadioButton rbt = (RadioButton) radioGroup.getChildAt(i);
            if (rbt.getText().equals(text))
                rbt.setChecked(true);
        }
    }

    private void setCheckedRadioButton(RadioGroup radioGroup, String[] list, String text, int index) {

        for (int i = 0; i < list.length; i++) {
            if (list[i].equals(text)) {
                RadioButton rbt = (RadioButton) radioGroup.getChildAt(i);
                rbt.setChecked(true);
                checkedID[index] = rbt.getId();
            }
        }
    }

    private void getCheckedFromCheckBox_Selection3() {
        list_selection_3 = new ArrayList<>();

        String[] customerIndustry = profileModel.getCustomerIndustry();

        if (chk40.isChecked())
            list_selection_3.add(customerIndustry[0].toString());
        if (chk41.isChecked())
            list_selection_3.add(customerIndustry[1].toString());
        if (chk42.isChecked())
            list_selection_3.add(customerIndustry[2].toString());
        if (chk43.isChecked())
            list_selection_3.add(customerIndustry[3].toString());
        if (chk44.isChecked())
            list_selection_3.add(customerIndustry[4].toString());
        if (chk45.isChecked())
            list_selection_3.add(customerIndustry[5].toString());
        if (chk46.isChecked())
            list_selection_3.add(customerIndustry[6].toString());
        if (chk47.isChecked())
            list_selection_3.add(customerIndustry[7].toString());
        if (chk48.isChecked())
            list_selection_3.add(customerIndustry[8].toString());
        if (chk49.isChecked())
            list_selection_3.add(customerIndustry[9].toString());
        if (chk410.isChecked())
            list_selection_3.add(customerIndustry[10].toString());
    }

    private void getCheckedFromCheckBox_Selection6() {
        list_selection_6 = new ArrayList<>();
        /*
        if (chk70.isChecked()) list_selection_6.add(chk70.getText().toString());
        if (chk71.isChecked()) list_selection_6.add(chk71.getText().toString());
        if (chk72.isChecked()) list_selection_6.add(chk72.getText().toString());
        if (chk73.isChecked()) list_selection_6.add(chk73.getText().toString());
        if (chk74.isChecked()) list_selection_6.add(chk74.getText().toString());
        if (chk75.isChecked()) list_selection_6.add(chk75.getText().toString());
        */
        String[] companyServices = profileModel.getCompanyService();

        if (chk70.isChecked())
            list_selection_6.add(companyServices[0].toString());
        if (chk71.isChecked())
            list_selection_6.add(companyServices[1].toString());
        if (chk72.isChecked())
            list_selection_6.add(companyServices[2].toString());
        if (chk73.isChecked())
            list_selection_6.add(companyServices[3].toString());
        if (chk74.isChecked())
            list_selection_6.add(companyServices[4].toString());
        if (chk75.isChecked())
            list_selection_6.add(companyServices[5].toString());
    }

    private void getCheckedFromCheckBox_Selection11() {
        list_selection_11 = new ArrayList<>();
        //if (chk100.isChecked()) list_selection_11.add(chk100.getText().toString());
        //if (chk101.isChecked()) list_selection_11.add(chk101.getText().toString());
        //if (chk102.isChecked()) list_selection_11.add(chk102.getText().toString());
        //if (chk103.isChecked()) list_selection_11.add(chk103.getText().toString());
        //if (chk104.isChecked()) list_selection_11.add(chk104.getText().toString());
        //if (chk105.isChecked()) list_selection_11.add(chk105.getText().toString());
        //if (chk106.isChecked()) list_selection_11.add(chk106.getText().toString());
        //if (chk107.isChecked()) list_selection_11.add(chk107.getText().toString());
        //if (chk108.isChecked()) list_selection_11.add(chk108.getText().toString());
        //if (chk109.isChecked()) list_selection_11.add(chk109.getText().toString());

        String[] keyAreas = profileModel.getKeyAreas();

        if (chk100.isChecked())
            list_selection_11.add(keyAreas[0].toString());
        if (chk101.isChecked())
            list_selection_11.add(keyAreas[1].toString());
        if (chk102.isChecked())
            list_selection_11.add(keyAreas[2].toString());
        if (chk103.isChecked())
            list_selection_11.add(keyAreas[3].toString());
        if (chk104.isChecked())
            list_selection_11.add(keyAreas[4].toString());
        if (chk105.isChecked())
            list_selection_11.add(keyAreas[5].toString());
        if (chk106.isChecked())
            list_selection_11.add(keyAreas[6].toString());
        if (chk107.isChecked())
            list_selection_11.add(keyAreas[7].toString());
        if (chk108.isChecked())
            list_selection_11.add(keyAreas[8].toString());
        if (chk109.isChecked())
            list_selection_11.add(keyAreas[9].toString());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick({R.id.img_profile_picture, R.id.tv_country, R.id.tv_city, R.id.profile_disclaimer})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_profile_picture:
                CharSequence colors[] = new CharSequence[]{getString(R.string.dialog_select_profile_picture_camera), getString(R.string.dialog_select_profile_picture_gallery)};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.dialog_select_profile_picture_dialog));
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // if (which == 0)
                        //getProfilePictureCallback.takePicture();
                        ///else
                        //getProfilePictureCallback.getPicture();
                    }
                });
                builder.show();
                break;
            case R.id.tv_country:
                getCountries();
                break;
            case R.id.tv_city:
                getCities();
                break;
            case R.id.profile_disclaimer:
                //app.changeFragment(new ContactUs(), true, "contact_us");
                break;
        }
    }


    public void onNext() {
        int percent = 0;
        if (tag == 8 || tag == 9)
            checkedID = new int[2];
        else
            checkedID = new int[1];

        Boolean proceed = false;

        String error_no_item_selected = getStringFromResources(R.string.dialog_select_one);
        String error_empty = getStringFromResources(R.string.error_empty_field);
        String error = error_no_item_selected;

        getCheckedFromCheckBox_Selection3();
        getCheckedFromCheckBox_Selection6();
        getCheckedFromCheckBox_Selection11();

        if (tag == 1) {
            error = error_empty;
            saveDataToModel();
            proceed = checkIfNull();
            setRadioGroupTags(radioGroup2, profileModel.getPrimaryJobFocus());
            setRadioButtonClickedListener(radioGroup2, 0);

            if (profileModel.getAccountType().equals(INTERNAL)) {
                mHomeActivity.setRightButton(R.drawable.ic_done);
            }
            mHomeActivity.setLeftButton(R.drawable.ic_back);
        } else if (tag == 2) {
            saveDataToModel();
            proceed = true;
            setRadioGroupTags(radioGroup3, profileModel.getContactType());
            setRadioButtonClickedListener(radioGroup3, 0);

            if (profileModel.getAccountType().equals(INTERNAL)) {
                mPresenter.updateProfile((AppCompatActivity) getActivity(), profileModel);

                dialog = null;
                dialog = mUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_saving_profile));
                dialog.show();
            }


        } else if (tag == 3) {
            proceed = true;
        } else if (tag == 4) {
            proceed = true;
            profileModel.setSelection3(list_selection_3);
            setRadioGroupTags(radioGroup5, profileModel.getJobFunction());
            setRadioButtonClickedListener(radioGroup5, 0);
        } else if (tag == 5) {
            proceed = true;
        } else if (tag == 6) {
            proceed = true;
        } else if (tag == 7) {
            proceed = true;
            profileModel.setSelection6(list_selection_6);
            setRadioGroupTags(radioGroup80, profileModel.getPreferredLanguage());
            setRadioGroupTags(radioGroup81, profileModel.getYearsOfXPInHVACR());
            setRadioButtonClickedListener(radioGroup80, 0);
            setRadioButtonClickedListener(radioGroup81, 1);
        } else if (tag == 8) {
            proceed = true;
            setRadioGroupTags(radioGroup90, profileModel.getUserMobileOS());
            setRadioGroupTags(radioGroup91, profileModel.getUserHasMobileConnection());
            setRadioButtonClickedListener(radioGroup90, 0);
            setRadioButtonClickedListener(radioGroup91, 1);
        } else if (tag == 9) {
            proceed = true;
            mHomeActivity.setRightButton(R.drawable.ic_done);

        } else if (tag == 10) {
            proceed = true;
            profileModel.setSelection11(list_selection_11);

            progressBar.setProgress(setPercent());

            mPresenter.updateProfile((AppCompatActivity) getActivity(), profileModel);

            dialog = null;
            dialog = mUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_saving_profile));
            dialog.show();
        } else {
            proceed = false;
        }

        Log.i("-- tag onNext", String.valueOf(tag));
        Log.i("-- list_selection_3", String.valueOf(list_selection_3.size()));
        Log.i("-- proceed", String.valueOf(proceed));

        if (!proceed) {
            dialog = mUtils.getNeutralDialog(getStringFromResources(R.string.error_error_occur), error, new Runnable() {
                @Override
                public void run() {
                    dialog.dismissWithAnimation();
                }
            }, SweetAlertDialog.ERROR_TYPE);

        }
        if (proceed) {
            if (tag < 10) {
                tag++;
                if (tag == 6 && profileModel.getSelection4().equals("")) {
                    tag++;
                }

                if (tag == 4 && !profileModel.getSelection2().equals("")) {
                    //Question for #4
                    if (profileModel.getSelection2().equals(getStringFromResources(R.string.profile_check_30)))
                        tvQuestion4.setText(R.string.profile_question_12a);
                    else if (profileModel.getSelection2().equals(getStringFromResources(R.string.profile_check_30)) || profileModel.getSelection2().equals(getStringFromResources(R.string.profile_check_32)) || profileModel.getSelection2().equals(getStringFromResources(R.string.profile_check_35)) || profileModel.getSelection2().equals(getStringFromResources(R.string.profile_check_31)))
                        tvQuestion4.setText(R.string.profile_question_12b);
                    else if (profileModel.getSelection2().equals(getStringFromResources(R.string.profile_check_33)) || profileModel.getSelection2().equals(getStringFromResources(R.string.profile_check_34)))
                        tvQuestion4.setText(R.string.profile_question_12c);
                    else
                        tvQuestion4.setText(R.string.profile_question_12d);
                } else if (tag == 4 && profileModel.getSelection2().equals("")) {
                    tag++;
                    profileModel.setSelection3(list_selection_3);
                    setRadioGroupTags(radioGroup5, profileModel.getJobFunction());
                    setRadioButtonClickedListener(radioGroup5, 0);
                }

                changeLayout();
            } else {
                Log.i("-- SAVE DATA", profileModel.toString());

            }
        } else {
            dialog.show();
        }

        if (tag > 10)
            mHomeActivity.setRightButton(R.drawable.ic_next);

        Log.i("-- profile", profileModel.toString());

//        SharedPreferences.Editor editor = app.getEditor();
//        Set<String> set3 = new HashSet<String>();
//        Set<String> set6 = new HashSet<String>();
//        Set<String> set11 = new HashSet<String>();
//        set3.addAll(profileModel.getSelection3());
//        set6.addAll(profileModel.getSelection6());
//        set11.addAll(profileModel.getSelection11());
//        editor.putStringSet(Preferences.PROFILE_SELECTION_3, set3);
//        editor.putStringSet(Preferences.PROFILE_SELECTION_6, set6);
//        editor.putStringSet(Preferences.PROFILE_SELECTION_11, set11);
//
//        editor.commit();
    }


    public String getStringFromResources(int id) {
        return getResources().getString(id);
    }

    public void onPrevious() {

        if (tag == 1) {
            dialog = mUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_logging_out));
            dialog.show();
            mPresenter.logout(getActivity());
        } else {
            tag--;

            if (tag == 1){
                mHomeActivity.setLeftButtonVisibility(View.VISIBLE);
                mHomeActivity.setLeftButton(R.drawable.ic_logout);
            }

            if (tag == 8 || tag == 9)
                checkedID = new int[2];
            else
                checkedID = new int[1];

            if (tag == 6 && profileModel.getSelection4().equals("")) {
                tag--;
            }
            if (tag == 4 && TextUtils.isEmpty(profileModel.getSelection2())) {
                tag--;
            }
            mHomeActivity.setRightButton(R.drawable.ic_next);
            changeLayoutonPrevious();
        }

    }


    public void changeLayout() {
        int percent = 0;

        if (tag == 8 || tag == 9)
            checkedID = new int[2];
        else
            checkedID = new int[1];

        Log.i("--tag change", "tag: " + tag);

        relProfile1.setVisibility(View.GONE);
        relProfile2.setVisibility(View.GONE);
        relProfile3.setVisibility(View.GONE);
        relProfile4.setVisibility(View.GONE);
        relProfile5.setVisibility(View.GONE);
        relProfile6.setVisibility(View.GONE);
        relProfile7.setVisibility(View.GONE);
        relProfile8.setVisibility(View.GONE);
        relProfile9.setVisibility(View.GONE);
        relProfile10.setVisibility(View.GONE);

        if (tag == 1) {
            relProfile1.setVisibility(View.VISIBLE);
            mHomeActivity.setLeftButton(R.drawable.ic_logout);
            mHomeActivity.setRightButtonVisibility(View.VISIBLE);
            mHomeActivity.setRightButton(R.drawable.ic_next);
        } else if (tag == 2) {
            resetCitiesArray();
            relProfile2.setVisibility(View.VISIBLE);
            //setCheckedRadioButtonFromRadioGroup(radioGroup2, profileModel.getSelection1());
            setCheckedRadioButton(radioGroup2, profileModel.getPrimaryJobFocus(), profileModel.getSelection1(), 0);
        } else if (tag == 3 && max_screens == 2) {
            relProfile2.setVisibility(View.VISIBLE);
        } else if (tag == 3 && max_screens != 2) {
            relProfile3.setVisibility(View.VISIBLE);
            //setCheckedRadioButtonFromRadioGroup(radioGroup3, profileModel.getSelection2());
            setCheckedRadioButton(radioGroup3, profileModel.getContactType(), profileModel.getSelection2(), 0);
        } else if (tag == 4) {
            // setCheckedCheckBox();
            relProfile4.setVisibility(View.VISIBLE);
        } else if (tag == 5) {
            relProfile5.setVisibility(View.VISIBLE);
            //setCheckedRadioButtonFromRadioGroup(radioGroup5, profileModel.getSelection4());
            setCheckedRadioButton(radioGroup5, profileModel.getJobFunction(), profileModel.getSelection4(), 0);
        } else if (tag == 6) {
            if (profileModel.getSelection4() != null) {
                relProfile6.setVisibility(View.VISIBLE);
                //setCheckedRadioButtonFromRadioGroup(radioGroup6, profileModel.getSelection5());

                switch (profileModel.getSelection4()) {
                    case "Education & Training":
                        setCheckedRadioButton(radioGroup6, profileModel.getEducationAndTrainingRole(), profileModel.getSelection5(), 0);
                        break;
                    case "Energy Management":
                        setCheckedRadioButton(radioGroup6, profileModel.getEnergyManagementRole(), profileModel.getSelection5(), 0);
                        break;
                    case "Engineering":
                        setCheckedRadioButton(radioGroup6, profileModel.getEngineeringRoles(), profileModel.getSelection5(), 0);
                        break;
                    case "HACCP":
                        setCheckedRadioButton(radioGroup6, profileModel.getHACCPRole(), profileModel.getSelection5(), 0);
                        break;
                    case "Industry / Trade Associations":
                        setCheckedRadioButton(radioGroup6, profileModel.getIndustryTradeAssociationRoles(), profileModel.getSelection5(), 0);
                        break;
                    case "Operations & Management":
                        setCheckedRadioButton(radioGroup6, profileModel.getOperationAndManagementRole(), profileModel.getSelection5(), 0);
                        break;
                    case "Projects":
                        setCheckedRadioButton(radioGroup6, profileModel.getProjectRole(), profileModel.getSelection5(), 0);
                        break;
                    case "Service / Repair / Maintenance":
                        setCheckedRadioButton(radioGroup6, profileModel.getServiceRepairMaintenanceRoles(), profileModel.getSelection5(), 0);
                        break;
                    case "Sales & Marketing":
                        setCheckedRadioButton(radioGroup6, profileModel.getSalesAndMarketingRole(), profileModel.getSelection5(), 0);
                        break;
                    case "Technical Services":
                        setCheckedRadioButton(radioGroup6, profileModel.getTechnicalServicesRole(), profileModel.getSelection5(), 0);
                        break;
                }
            }
        } else if (tag == 7) {
            //setCheckedCheckBox();
            relProfile7.setVisibility(View.VISIBLE);
        } else if (tag == 8) {
            relProfile8.setVisibility(View.VISIBLE);
            //setCheckedRadioButtonFromRadioGroup(radioGroup80, profileModel.getSelection8());
            setCheckedRadioButton(radioGroup80, profileModel.getPreferredLanguage(), profileModel.getSelection8(), 0);
            //setCheckedRadioButtonFromRadioGroup(radioGroup81, profileModel.getSelection7());
            setCheckedRadioButton(radioGroup81, profileModel.getYearsOfXPInHVACR(), profileModel.getSelection7(), 1);
        } else if (tag == 9) {
            relProfile9.setVisibility(View.VISIBLE);
            //setCheckedRadioButtonFromRadioGroup(radioGroup90, profileModel.getSelection9());
            //setCheckedRadioButtonFromRadioGroup(radioGroup91, profileModel.getSelection10());
            setCheckedRadioButton(radioGroup90, profileModel.getUserMobileOS(), profileModel.getSelection9(), 0);
            setCheckedRadioButton(radioGroup91, profileModel.getUserHasMobileConnection(), profileModel.getSelection10(), 1);
        } else if (tag == 10) {
            //setCheckedCheckBox();
            relProfile10.setVisibility(View.VISIBLE);
        }


        percent = setPercent();
        progressBar.setProgress(percent);

        Log.i("-- current tag", String.valueOf(tag));
    }

    public void changeLayoutonPrevious() {
        int percent = 0;

        relProfile1.setVisibility(View.GONE);
        relProfile2.setVisibility(View.GONE);
        relProfile3.setVisibility(View.GONE);
        relProfile4.setVisibility(View.GONE);
        relProfile5.setVisibility(View.GONE);
        relProfile6.setVisibility(View.GONE);
        relProfile7.setVisibility(View.GONE);
        relProfile8.setVisibility(View.GONE);
        relProfile9.setVisibility(View.GONE);
        relProfile10.setVisibility(View.GONE);

        if (tag == 1) {
            relProfile1.setVisibility(View.VISIBLE);
        } else if (tag == 2) {
            relProfile2.setVisibility(View.VISIBLE);
        } else if (tag == 3) {
            relProfile3.setVisibility(View.VISIBLE);
        } else if (tag == 4) {
            relProfile4.setVisibility(View.VISIBLE);
        } else if (tag == 5) {
            relProfile5.setVisibility(View.VISIBLE);
        } else if (tag == 6) {
            relProfile6.setVisibility(View.VISIBLE);
        } else if (tag == 7) {
            relProfile7.setVisibility(View.VISIBLE);
        } else if (tag == 8) {
            relProfile8.setVisibility(View.VISIBLE);
        } else if (tag == 9) {
            relProfile9.setVisibility(View.VISIBLE);
        } else if (tag == 10) {
            relProfile10.setVisibility(View.VISIBLE);
        }


    }

    //Dynamic RadioButtons for layout6
    public void dynamicRadioButtons() {
        String[] rbt_texts = null;
        String[] rbt_tags = null;
        switch (radioGroup6.getTag().toString()) {
            case "Education & Training":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6a", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getEducationAndTrainingRole();
                break;
            case "Energy Management":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6b", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getEnergyManagementRole();
                break;
            case "Engineering":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6c", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getEngineeringRoles();
                break;
            case "HACCP":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6d", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getHACCPRole();
                break;
            case "Industry / Trade Associations":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6e", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getIndustryTradeAssociationRoles();
                break;
            case "Operations / Management":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6f", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getOperationAndManagementRole();
                break;
            case "Projects":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6g", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getProjectRole();
                break;
            case "Service / Repair / Maintenance":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6h", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getServiceRepairMaintenanceRoles();
                break;
            case "Sales & Marketing":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6i", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getSalesAndMarketingRole();
                break;
            case "Technical Services":
                rbt_texts = getResources().getStringArray(getResources().getIdentifier("profile_check_6j", "array", getActivity().getPackageName()));
                rbt_tags = profileModel.getTechnicalServicesRole();
                break;
        }
        //RemoveViews from RadioGroup6
        radioGroup6.removeAllViews();
        //AddViews to RadioGroup6
        if (rbt_texts != null) {
            for (int i = 0; i < rbt_texts.length; i++) {
                RadioButton radioButton = (RadioButton) LayoutInflater.from(getContext()).inflate(R.layout.radio_button_item, null);
                radioButton.setText(rbt_texts[i]);
                radioButton.setId(i);
                radioButton.setTag(rbt_tags[i]);
                radioGroup6.addView(radioButton);
            }
        }
        setRadioButtonClickedListener(radioGroup6, 0);
        radioGroup6.requestLayout();


    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        RadioButton rbt = (RadioButton) view.findViewById(i);

        if (radioGroup == radioGroup2) {

            for (int ctr = 0; ctr < radioGroup2.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection1("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 1", (profileModel.getPrimaryJobFocus())[ctr].toString());
                }
            }


        } else if (radioGroup == radioGroup3) {

            for (int ctr = 0; ctr < radioGroup3.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection2("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 2", (profileModel.getContactType())[ctr].toString());
                }
            }

        } else if (radioGroup == radioGroup5) {

            for (int ctr = 0; ctr < radioGroup5.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection4("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 5", (profileModel.getJobFunction())[ctr].toString());
                }
            }
        } else if (radioGroup == radioGroup6) {

            String[] mainRoles = null;

            switch (profileModel.getSelection4()) {
                case "Education & Training":
                    mainRoles = profileModel.getEducationAndTrainingRole();
                    break;
                case "Energy Management":
                    mainRoles = profileModel.getEnergyManagementRole();
                    break;
                case "Engineering":
                    mainRoles = profileModel.getEngineeringRoles();
                    break;
                case "HACCP":
                    mainRoles = profileModel.getHACCPRole();
                    break;
                case "Industry / Trade Associations":
                    mainRoles = profileModel.getIndustryTradeAssociationRoles();
                    break;
                case "Operations / Management":
                    mainRoles = profileModel.getOperationAndManagementRole();
                    break;
                case "Projects":
                    mainRoles = profileModel.getProjectRole();
                    break;
                case "Service / Repair / Maintenance":
                    mainRoles = profileModel.getServiceRepairMaintenanceRoles();
                    break;
                case "Sales & Marketing":
                    mainRoles = profileModel.getSalesAndMarketingRole();
                    break;
                case "Technical Services":
                    mainRoles = profileModel.getTechnicalServicesRole();
                    break;
            }

            for (int ctr = 0; ctr < radioGroup6.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection5("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 6", mainRoles[ctr].toString());
                }
            }

        } else if (radioGroup == radioGroup80) {

            for (int ctr = 0; ctr < radioGroup80.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection8("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 8", (profileModel.getPreferredLanguage())[ctr].toString());
                }
            }
        } else if (radioGroup == radioGroup81) {

            for (int ctr = 0; ctr < radioGroup81.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection7("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 7", (profileModel.getYearsOfXPInHVACR())[ctr].toString());
                }
            }
        } else if (radioGroup == radioGroup90) {

            for (int ctr = 0; ctr < radioGroup90.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection9("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 9", (profileModel.getUserMobileOS())[ctr].toString());
                }
            }
        } else if (radioGroup == radioGroup91) {

            for (int ctr = 0; ctr < radioGroup91.getChildCount(); ctr++) {
                RadioButton rbtn = (RadioButton) radioGroup.getChildAt(ctr);

                if (rbtn.isChecked()) {
                    profileModel.setSelection10("" + rbtn.getTag());
                    Log.i("-- onCheckedChanged 91", (profileModel.getUserHasMobileConnection())[ctr].toString());
                }
            }
        }

        if (this.tag == 5) {
            if (rbt != null) {
                Log.i("--tag", rbt.getTag().toString());
                radioGroup6.setTag(rbt.getTag().toString());
                dynamicRadioButtons();
            }
        }

        Log.i("-- profile", profileModel.toString());
    }

    private void fillArrayCheckBox() {
        list_checkbox_3 = new ArrayList<>();
        list_checkbox_3.add(chk40);
        list_checkbox_3.add(chk41);
        list_checkbox_3.add(chk42);
        list_checkbox_3.add(chk43);
        list_checkbox_3.add(chk44);
        list_checkbox_3.add(chk45);
        list_checkbox_3.add(chk46);
        list_checkbox_3.add(chk47);
        list_checkbox_3.add(chk48);
        list_checkbox_3.add(chk49);
        list_checkbox_3.add(chk410);

        list_checkbox_6 = new ArrayList<>();
        list_checkbox_6.add(chk70);
        list_checkbox_6.add(chk71);
        list_checkbox_6.add(chk72);
        list_checkbox_6.add(chk73);
        list_checkbox_6.add(chk74);
        list_checkbox_6.add(chk75);

        list_checkbox_11 = new ArrayList<>();
        list_checkbox_11.add(chk100);
        list_checkbox_11.add(chk101);
        list_checkbox_11.add(chk102);
        list_checkbox_11.add(chk103);
        list_checkbox_11.add(chk104);
        list_checkbox_11.add(chk105);
        list_checkbox_11.add(chk106);
        list_checkbox_11.add(chk107);
        list_checkbox_11.add(chk108);
        list_checkbox_11.add(chk109);
    }

    private void setCheckedCheckBox() {
        //if (tag == 4) {
        for (int i = 0; i < profileModel.getSelection3().size(); i++) {
            String text = profileModel.getSelection3().get(i);
            Log.d("TAG", text);
            int index = getIndexFromStringList(profileModel.getCustomerIndustry(), text.trim());
            if (index != -1) {
                CheckBox checkBox = list_checkbox_3.get(index);
                checkBox.setChecked(true);
            }
        }
        //} else if (tag == 7) {
        for (int i = 0; i < profileModel.getSelection6().size(); i++) {
            int index = getIndexFromStringList(profileModel.getCompanyService(), (profileModel.getSelection6().get(i)).trim());
            if (index != -1) {
                CheckBox checkBox = list_checkbox_6.get(index);
                checkBox.setChecked(true);
            }
        }
        //  } else if (tag == 10) {
//            if (profileModel.getSelection11().size() > 0) {
//                progressBar.setProgress(100);
//            }

        for (int i = 0; i < profileModel.getSelection11().size(); i++) {
            int index = getIndexFromStringList(profileModel.getKeyAreas(), profileModel.getSelection11().get(i).trim());
            if (index != -1) {
                CheckBox checkBox = list_checkbox_11.get(index);
                checkBox.setChecked(true);
            }
        }
        // }
    }

    public Boolean checkIfNull() {
        Boolean bool = true;

        if (tag == 1) {
            if (checkIfNull(profileModel.getC_FirstName()))
                bool = false;
            else if (checkIfNull(profileModel.getC_LastName()))
                bool = false;
            else if (checkIfNull(profileModel.getC_Phone()))
                bool = false;
            else if (checkIfNull(profileModel.getA_Country()))
                bool = false;
            else if (checkIfNull(profileModel.getU_EmailAddress()))
                bool = false;
            else if (checkIfNull(profileModel.getA_Name()))
                bool = false;
//            else if (checkIfNull(profileModel.getA_City()))
//                bool = false;
        } else if (tag == 2) {
            if (checkIfNull(profileModel.getA_Address()))
                bool = false;
            else if (checkIfNull(profileModel.getJobTitle()))
                bool = false;
        }

        bool = true;

        return bool;
    }

    private Boolean checkIfNull(String s) {
        return TextUtils.isEmpty(s);
    }

    public void saveDataToModel() {
        if (tag == 1) {
            profileModel.setC_LastName(etLastName.getText().toString());
            profileModel.setC_FirstName(etFirstName.getText().toString());
            profileModel.setA_Address(etBusinessAddress.getText().toString());
            profileModel.setA_Name(etCompanyName.getText().toString());
            profileModel.setC_Phone(etMobile.getText().toString());
            profileModel.setA_Country(tvCountry.getText().toString());
            profileModel.setU_EmailAddress(etEmail.getText().toString());
        } else if (tag == 2) {
            profileModel.setA_City(tvCity.getText().toString());
            profileModel.setJobTitle(etJobTitle.getText().toString());
            profileModel.setA_Address(etBusinessAddress.getText().toString());
        } else if (tag == 3) {

        } else if (tag == 10) {

        }
    }

    public void resetCitiesArray() {
        try {
            cities = new ArrayList<>();
            JSONObject jsonObject1 = new JSONObject(mUtils.readTxt(R.raw.city));
            JSONArray jsonObject2 = jsonObject1.getJSONArray(profileModel.getA_Country());
            for (int i = 0; i < jsonObject2.length(); i++) {
                cities.add(jsonObject2.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setProfilePicture(String path) {
        File file = new File(path);
        if (file.exists()) {
            mUtils.cacheImage(imgProfilePicture, ImageDownloader.Scheme.FILE.wrap(path));
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            Log.i("*** take ***", path);
            editor.putString(Preferences.PROFILE_PICTUREPATH, ImageDownloader.Scheme.FILE.wrap(path));
            editor.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        callback.setRightMenuTag(1, View.VISIBLE);
//        Log.i("--right", "21");
//        callback.setLeftMenuTag(1, View.GONE);
//        callback.enableFullScreen(false,false);
//        app.setShowSocial(false);
//        app.setMessageListShown(false);
    }

    private int setPercent() {
        int p = 0;

        if (max_screens == 2) {
            for (int i = 50; i <= 100; i = i + 50) {
                if (i == 50) {
                    p = i;
                } else if (i == 100 && !checkIfNull(profileModel.getA_City()) && !checkIfNull(profileModel.getJobTitle())) {
                    p = i;
                }
            }
        } else {
            p = 10;
            if (!checkIfNull(profileModel.getA_City()) && !checkIfNull(profileModel.getJobTitle())) {
                p += 10;
            }
            if (!TextUtils.isEmpty(profileModel.getSelection2())) {
                p += 10;
            }
            getCheckedFromCheckBox_Selection3();
            if (list_selection_3.size() > 0) {
                p += 10;
            }
            if (!TextUtils.isEmpty(profileModel.getSelection4())) {
                p += 10;
            }
            if (!TextUtils.isEmpty(profileModel.getSelection5())) {
                p += 10;
            }
            getCheckedFromCheckBox_Selection6();
            if (list_selection_6.size() > 0) {
                p += 10;
            }
            if (!TextUtils.isEmpty(profileModel.getSelection7()) && !TextUtils.isEmpty(profileModel.getSelection8())) {
                p += 10;
            }
            if (!TextUtils.isEmpty(profileModel.getSelection9()) && !TextUtils.isEmpty(profileModel.getSelection10())) {
                p += 10;
            }
            getCheckedFromCheckBox_Selection11();
            if (list_selection_11.size() > 0) {
                p += 10;
            }

        }

        return p;
    }


    public void setProgressBar(int status) {
        if (status == 1) {
            dialog.show();
        }
    }

    public void setupData() {

        Gson gson = new Gson();
        countries = gson.fromJson(mUtils.readTxt(R.raw.phone), new TypeToken<ArrayList<Countries>>() {
        }.getType());
        profileModel = app.getProfileModel();
        String contactNumber = profileModel.getC_Phone();
        for (Countries c : countries) {
            if (c.getName().equals(profileModel.getA_Country())) {
                tvDialCode.setText(c.getDial_code());
                contactNumber = profileModel.getC_Phone().replace(c.getDial_code(), "");
                contactNumber = contactNumber.replace("+", "");
                contactNumber = contactNumber.replace(" ", "");
            }
        }

        etLastName.setText(profileModel.getC_LastName());
        etFirstName.setText(profileModel.getC_FirstName());
        etBusinessAddress.setText(profileModel.getA_Address());
        etCompanyName.setText(profileModel.getA_Name());
        etMobile.setText(contactNumber);
        tvCountry.setText(profileModel.getA_Country());
        tvCity.setText(profileModel.getA_City());
        etEmail.setText(profileModel.getU_EmailAddress());
        etJobTitle.setText(profileModel.getJobTitle());


        if (profileModel.getAccountType().equals(INTERNAL)) {
            max_screens = 2;
        } else {
            max_screens = 10;
        }

        setCheckedCheckBox();
        changeLayout();
    }


    public void onSuccessUpdateProfile() {

        if (dialog.isShowing())
            dialog.dismiss();

        tag = 1;
        setupData();

        dialog.setTitleText("Message")
                .setContentText(getResources().getString(R.string.profile_success))
                .setConfirmText(getResources().getString(R.string.dialog_ok))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmClickListener(null)
                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        dialog.show();

    }

    public void onFailureUpdateProfile(String errorMessage) {

        if (dialog.isShowing())
            dialog.dismiss();

        dialog.setTitleText("Message")
                .setContentText(errorMessage)
                .setConfirmText(getResources().getString(R.string.dialog_ok))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmClickListener(null)
                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
        dialog.show();
    }


    private int getIndexFromStringList(String[] stringList, String textToCompare) {
        int index = -1;
        for (int i = 0; i < stringList.length; i++) {
            if (stringList[i].equals(textToCompare)) {
                return i;
            }
        }

        return index;
    }

    private void setRadioGroupTags(RadioGroup radioGroup, String[] tags) {
        int count = radioGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View o = radioGroup.getChildAt(i);
            if (o instanceof RadioButton) {
                o.setTag(tags[i]);
            }
        }
    }

    private void setRadioButtonClickedListener(final RadioGroup radioGroup, final int index) {
        int count = radioGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            final RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
            radioButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    RadioButton rb = (RadioButton) view;
                    if (rb.getId() == checkedID[index]) {
                        radioGroup.clearCheck();
                        checkedID[index] = -1;
                        if (radioGroup == radioGroup2) {
                            profileModel.setSelection1("");
                        } else if (radioGroup == radioGroup3) {
                            profileModel.setSelection2("");
                        } else if (radioGroup == radioGroup5) {
                            profileModel.setSelection4("");
                            profileModel.setSelection5("");
                        } else if (radioGroup == radioGroup6) {
                            profileModel.setSelection5("");
                        } else if (radioGroup == radioGroup80) {
                            profileModel.setSelection8("");
                        } else if (radioGroup == radioGroup81) {
                            profileModel.setSelection7("");
                        } else if (radioGroup == radioGroup90) {
                            profileModel.setSelection9("");
                        } else if (radioGroup == radioGroup91) {
                            profileModel.setSelection10("");
                        }
                    } else {
                        checkedID[index] = rb.getId();
                    }
                }
            });
        }
    }

    @Override
    public void onSuccessLogout() {
        if (dialog.isShowing())
            dialog.dismiss();

        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    @Override
    public void onErrorLogout() {
        dialog.dismiss();

        dialog = mUtils.getNeutralDialog(getResources().getString(R.string.error_error_occur), getResources().getString(R.string.error_no_internet), new Runnable() {
            @Override
            public void run() {
                dialog.dismissWithAnimation();
            }
        }, SweetAlertDialog.ERROR_TYPE);
        dialog.show();
    }

    @Override
    public void onLeftButtonClicked() {
        onPrevious();
    }

    @Override
    public void onRightButtonClicked() {
        onNext();
    }


}
