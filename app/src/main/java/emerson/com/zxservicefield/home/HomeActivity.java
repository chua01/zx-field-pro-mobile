package emerson.com.zxservicefield.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.customviews.TabBarView;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.home.profile.ProfileFragment;
import emerson.com.zxservicefield.util.AppUtils;

/**
 * Created by mobileapps on 10/26/16.
 */

public class HomeActivity extends AppCompatActivity implements HomeContract.View {

    @Bind(R.id.btnLeft)
    ImageView btnLeft;
    @Bind(R.id.btnRight)
    ImageView btnRight;
    @Bind(R.id.tab_bar)
    TabBarView mTabBarView;
    @Bind(R.id.tv_header_1)
    TextView mTitle;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.frameLayout)
    FrameLayout mFrameLayout;

    private HomeToolbar mHomeToolbar;
    private ProfileModel mProfileModel;
    private App mApp;
    private AppUtils mUtils;
    private SweetAlertDialog mDialog;

    @Inject
    HomePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        DaggerHomeComponent.builder()
                .netComponent(((App) getApplication()).getNetComponent())
                .homeModule(new HomeModule(this))
                .build().inject(this);



        mApp = (App) this.getApplication();

        mUtils = new AppUtils(this);

        mProfileModel = mApp.getProfileModel();
        mDialog = mUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_loading_profile));
        mDialog.show();


        mPresenter.getUser(this, mProfileModel);


    }

    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount != 0) {
            replaceFragment(mTabBarView.getActiveFragment());
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setToolbar(HomeToolbar toolbar) {
        mHomeToolbar = toolbar;
    }

    @Override
    public void setRightButton(int drawable) {
        btnRight.setImageResource(drawable);
    }

    @Override
    public void setLeftButton(int drawable) {
        btnLeft.setImageResource(drawable);
    }

    @Override
    public void setRightButtonVisibility(int visibility) {
        btnRight.setVisibility(visibility);
    }

    @Override
    public void setLeftButtonVisibility(int visibility) {
        btnLeft.setVisibility(visibility);
    }

    @Override
    public void setTabBarVisibility(int visibility) {
        mTabBarView.setVisibility(visibility);
    }

    @Override
    public void setColorBackground(int color) {
        mToolbar.setBackgroundColor(color);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void setTitle(String title) {
        mTitle.setText(title);
    }

    @Override
    public void onSuccess() {
        if (mDialog.isShowing())
            mDialog.dismiss();
        ProfileFragment profileFragment = new ProfileFragment();
        replaceFragment(profileFragment);
    }

    @Override
    public void onFailure(String errorMessage) {
        mDialog.setTitleText(getResources().getString(R.string.error_error_occur)).setContentText(errorMessage).setConfirmText(getResources().getString(R.string.dialog_ok)).changeAlertType(SweetAlertDialog.ERROR_TYPE);
    }

    @OnClick({R.id.btnRight, R.id.btnLeft})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRight:
                mHomeToolbar.onRightButtonClicked();
                break;
            case R.id.btnLeft:
                mHomeToolbar.onLeftButtonClicked();
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.commit();
    }
}
