package emerson.com.zxservicefield.home.qr.input;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.UnitModel;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.qr.verification.VerificationFragment;
import emerson.com.zxservicefield.util.AppUtils;

public class InputFragment extends Fragment {

    @Bind(R.id.et_model_num)
    EditText etModelNum;
    @Bind(R.id.et_serial_num)
    EditText etSerialNum;
    @Bind(R.id.et_customer)
    EditText etCustomer;
    @Bind(R.id.btn_continue)
    Button btnContinue;

    private String mSerialNum;
    private String mModelNum;
    private AppLocationService appLocationService;
    private double longitude = 0;
    private double latitude =  0;
    private String mCountry = "";
    private SweetAlertDialog sweetAlertDialog;
    private AppUtils mUtils;
    private String mLocation = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input, container, false);
        ButterKnife.bind(this,view);

        mUtils = new AppUtils(getActivity());

        appLocationService = new AppLocationService(getActivity());


        btnContinue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(etCustomer.getText().toString())){
                    setEmptyFieldsError();
                }else if(TextUtils.isEmpty(etModelNum.getText().toString())){
                    setEmptyFieldsError();
                }else if(TextUtils.isEmpty(etSerialNum.getText().toString())){
                    setEmptyFieldsError();
                }else{
                    UnitModel unitModel = new UnitModel();
                    unitModel.setCustomerName(etCustomer.getText().toString());
                    unitModel.setInstallationCompany("");
                    unitModel.setLocation(mLocation);
                    unitModel.setModelNumber(etModelNum.getText().toString());
                    unitModel.setSerialNumber(etSerialNum.getText().toString());
                    unitModel.setLocationLat(latitude);
                    unitModel.setLocationLong(longitude);
                    unitModel.setCountry(mCountry);

                    HomeActivity home = ((HomeActivity) getActivity());
                    home.setTabBarVisibility(View.GONE);

                    VerificationFragment verificationFragment = new VerificationFragment();
                    verificationFragment.setUnitModel(unitModel);
                    pushFragment(verificationFragment,"verification");

                }


            }
        });

        if (mModelNum != null){
            etModelNum.setText(mModelNum);
        }

        if (mSerialNum != null){
            etSerialNum.setText(mSerialNum);
        }

        Location location = appLocationService
                .getLocation(LocationManager.NETWORK_PROVIDER);

        //you can hard-code the lat & long if you have issues with getting it
        //remove the below if-condition and use the following couple of lines
        //double latitude = 37.422005;
        //double longitude = -122.084095

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude,
                    getActivity().getApplicationContext(), new GeocoderHandler());
        } else {
            showSettingsAlert();
        }

        return view;
    }

    private void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getActivity());
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getActivity().startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    public void setCode(String mCode) {
        String[] value = mCode.split(",");
        mModelNum = value[0];
        mSerialNum = value[1];
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            String country = "";
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    country = bundle.getString("country");
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = "";
            }
            mLocation = locationAddress;
            mCountry = country;
        }
    }

    private void pushFragment(Fragment fragment, String tag){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout,fragment,tag);
        ft.commit();
    }

    private void setProgressDialog() {
        sweetAlertDialog = mUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_register_account));
    }

    private void setEmptyFieldsError() {
        setProgressDialog();
        sweetAlertDialog.setTitleText("Message")
                .setContentText("Empty Fields")
                .setConfirmText(getResources().getString(R.string.dialog_ok))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmClickListener(null)
                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.show();
    }


}