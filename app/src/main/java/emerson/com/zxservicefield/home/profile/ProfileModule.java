package emerson.com.zxservicefield.home.profile;

import dagger.Module;
import dagger.Provides;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 10/28/16.
 */

@Module
public class ProfileModule {
    private final ProfileContract.View mView;


    public ProfileModule(ProfileContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    ProfileContract.View providesProfileContractView() {
        return mView;
    }
}