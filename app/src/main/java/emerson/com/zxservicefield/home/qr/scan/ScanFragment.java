package emerson.com.zxservicefield.home.qr.scan;

import android.Manifest.permission;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.qr.input.InputFragment;
import emerson.com.zxservicefield.util.AppUtils;

/**
 * Created by mobileapps on 11/2/16.
 */

public class ScanFragment extends Fragment {

    private static final String TAG = ScanFragment.class.getSimpleName();

    @Bind(R.id.camera_view)
    SurfaceView mCameraView;

    private boolean isProcessing;
    private View mView;
    private CameraSource mCameraSource;
    private AppUtils mUtils;
    private SweetAlertDialog mDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_scan, container, false);
        ButterKnife.bind(this, mView);


        mUtils = new AppUtils(getActivity());
        mDialog = mUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_loading_profile));


        ((HomeActivity) getActivity()).setTabBarVisibility(View.GONE);

        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(getActivity())
                        .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                        .build();

        mCameraSource = new CameraSource
                .Builder(getContext(), barcodeDetector)
                .build();

        mCameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(getContext(), permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mCameraSource.start(mCameraView.getHolder());
                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                mCameraSource.stop();
            }

        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {

            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes != null && barcodes.size() > 0) {
                    for (int i = 0; i < barcodes.size(); i++) {
                        if (!barcodes.valueAt(i).rawValue.equals(null) && !barcodes.valueAt(i).rawValue.equals("")) {
                            if (!isProcessing) {
                                Log.d(TAG, "QR Code :" + barcodes.valueAt(i).rawValue);
                                if (isCodeValid(barcodes.valueAt(i).rawValue)) {
                                    InputFragment inputFragment = new InputFragment();
                                    inputFragment.setCode(barcodes.valueAt(i).rawValue);
                                    pushFragment(inputFragment, "input");
                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (!mDialog.isShowing()) {
                                                mDialog.setTitleText(getResources().getString(R.string.message))
                                                        .setContentText(getResources().getString(R.string.dialog_invalid_code))
                                                        .setConfirmText(getResources().getString(R.string.dialog_ok))
                                                        .showCancelButton(false)
                                                        .setCancelClickListener(null)
                                                        .setConfirmClickListener(new OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                sweetAlertDialog.dismissWithAnimation();
                                                                getActivity().onBackPressed();
                                                            }
                                                        })
                                                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                mDialog.show();
                                            }
                                        }
                                    });


                                }
                                isProcessing = true;
                            }
                        }
                    }
                } else {
                    // Log.e(TAG,"SparseArray null or empty");
                }

            }
        });

        return mView;
    }

    @Override
    public void onDestroyView() {
        ((HomeActivity) getActivity()).setTabBarVisibility(View.VISIBLE);
        super.onDestroyView();
    }

    @OnClick({R.id.btn_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                getActivity().onBackPressed();
                break;
        }
    }

    private void pushFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment, tag);
        ft.commit();
    }

    private boolean isCodeValid(String code) {
        if (code.split(",").length >= 2) {
            return true;
        }

        isProcessing = false;

        return false;
    }
}
