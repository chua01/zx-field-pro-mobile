package emerson.com.zxservicefield.home;

import android.app.Activity;

import emerson.com.zxservicefield.data.response.ProfileModel;

/**
 * Created by mobileapps on 10/28/16.
 */

public interface HomeContract {

    interface View {
        void setToolbar(HomeToolbar toolbar);
        void setRightButton(int drawable);
        void setLeftButton(int drawable);
        void setRightButtonVisibility(int visibility);
        void setLeftButtonVisibility(int visibility);
        void setTabBarVisibility(int visibility);
        void setColorBackground(int color);
        void setTitle(String title);
        void onSuccess();
        void onFailure(String message);
    }

    interface Presenter {
        void getUser(Activity activity, ProfileModel profileModel);
    }

}
