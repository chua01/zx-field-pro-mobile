package emerson.com.zxservicefield.home.qr.step;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import butterknife.Bind;
import butterknife.ButterKnife;
import emerson.com.zxservicefield.R;

/**
 * Created by mobileapps on 11/15/16.
 */

public class VideoViewer extends Fragment {

    @Bind(R.id.videoView)
    VideoView mVideoView;
    private String mLink;

    public void setLink(String mLink) {
        this.mLink = mLink;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_viewer,container,false);
        ButterKnife.bind(this,view);

        Uri uriVideo = Uri.parse("http://www.androidbegin.com/tutorial/AndroidCommercial.3gp");

        MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(mVideoView);
        mVideoView.setMediaController(mediaController);
        mVideoView.setVideoURI(uriVideo);
        mVideoView.requestFocus();

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {
                mVideoView.start();
            }
        });

        return view;
    }
}
