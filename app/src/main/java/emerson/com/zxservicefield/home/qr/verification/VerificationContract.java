package emerson.com.zxservicefield.home.qr.verification;

import android.app.Activity;

import emerson.com.zxservicefield.data.response.UnitModel;

/**
 * Created by mobileapps on 11/7/16.
 */

public interface VerificationContract {

    interface View{
        void onSuccess(UnitModel unitModel);
        void onFailed();
    }

    interface Presenter{
        void verify(Activity activity,UnitModel unitModel);
    }

}
