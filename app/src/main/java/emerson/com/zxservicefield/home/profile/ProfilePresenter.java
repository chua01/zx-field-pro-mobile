package emerson.com.zxservicefield.home.profile;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.inject.Inject;

import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.Preferences;
import emerson.com.zxservicefield.data.response.ProfileCRMModel;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.data.response.ResponseObject;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mobileapps on 10/28/16.
 */

public class ProfilePresenter implements ProfileContract.Presenter {

    private ResponseObject mResponseObject;
    private ProfileContract.View mView;
    private Retrofit mRetrofit;
    private App mApp;
    private ProfileCRMModel profileCRMModel;
    private String mError;

    @Inject
    public ProfilePresenter(Retrofit retrofit, ProfileContract.View view) {
        this.mRetrofit = retrofit;
        this.mView = view;
    }


    @Override
    public void saveProfile(SharedPreferences sharedPreferences, ProfileModel profile) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("City", profile.getA_City());
        editor.putString("JobTitle", profile.getJobTitle());
        editor.putString("Selection1", profile.getSelection1());
        editor.putString("Selection2", profile.getSelection2());
        editor.putString("Selection3", profile.getSelection2());
        editor.putString("Selection4", profile.getSelection4());
        editor.putString("Selection5", profile.getSelection5());

        editor.commit();
    }

    @Override
    public void updateProfile(final Activity activity, final ProfileModel profile) {

        Observable<ResponseObject> call = mRetrofit.create(ProfileService.class).sendProfileToCRM(profile.getID(), profile.getContactId(), profile.getContactAccountId(), profile.getA_Address(), profile.getA_City(), profile.getJobTitle(), profile.getSelection1(), profile.getSelection2(), profile.getStringSelection3(), profile.getStringSelection3(), profile.getSelection4(), profile.getSelection5(), profile.getStringSelection6(), profile.getSelection7(), profile.getSelection8(), profile.getSelection9(), profile.getSelection10(), profile.getStringSelection11());
        call.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<ResponseObject>() {
            @Override
            public void onCompleted() {
                mError = mResponseObject.getErrorMessage();
                Log.i("*** onCompleted ***", "error: " + mResponseObject.getErrorMessage());
                if (!TextUtils.isEmpty(mError))
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mView.onFailureUpdateProfile(mError);
                        }
                    });
                else {
                    Gson gson = new Gson();
                    ((App)activity.getApplication()).setProfileModel(profile);
                    profileCRMModel = gson.fromJson(gson.toJson(mResponseObject.getResponseItem()), new TypeToken<ProfileCRMModel>() {
                    }.getType());
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mView.onSuccessUpdateProfile();
                        }
                    });

                }
            }

            @Override
            public void onError(Throwable e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mView.onFailureUpdateProfile(activity.getResources().getString(R.string.error_no_internet));
                    }
                });

            }

            @Override
            public void onNext(ResponseObject result) {
                Log.i("*** onNext ***", "next");
                mResponseObject = result;
            }
        });
    }

    @Override
    public void logout(final Activity activity) {
        mResponseObject = new ResponseObject();
        final App app = (App) activity.getApplication();
        Log.i("*** membershipID ***", "membershipID: " + app.getProfileModel().getID());
        Observable<ResponseObject> call = mRetrofit.create(ProfileService.class).submitMembershipIfLoginOrLogout(app.getProfileModel().getID(), false);
        call.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<ResponseObject>() {
            @Override
            public void onCompleted() {
                String error = mResponseObject.getErrorMessage();
                if (!TextUtils.isEmpty(error)) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mView.onErrorLogout();
                        }
                    });
                }else {
                    Gson gson = new Gson();
                    int response = gson.fromJson(gson.toJson(mResponseObject.getResponseItem()), new TypeToken<Integer>() {}.getType());
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Preferences.PROFILE_FIRSTNAME, "");
                    editor.putString(Preferences.PROFILE_EMAIL, "");
                    editor.putString(Preferences.PROFILE_ID, "");
                    editor.putString(Preferences.PROFILE_ACCOUNTID, "");
                    editor.putString(Preferences.PROFILE_CONTACTID, "");
                    editor.putString(Preferences.PROFILE_PHONE, "");
                    editor.putString(Preferences.PROFILE_LASTNAME, "");
                    editor.putString(Preferences.PROFILE_COUNTRY, "");
                    editor.putString(Preferences.PROFILE_CITY, "");
                    editor.putString(Preferences.PROFILE_ROLE, "");
                    editor.putString(Preferences.MESSAGES_1_SYNDATE, "");
                    editor.putString(Preferences.MESSAGES_2_SYNDATE, "");
                    editor.putString(Preferences.MESSAGES_3_SYNDATE, "");
                    editor.putString(Preferences.MESSAGES_4_SYNDATE, "");
                    editor.putBoolean(Preferences.SYNCONFIRSTLOGIN, false);
                    editor.putString(Preferences.PROFILE_PICTUREPATH, "");
                    editor.commit();

                    Log.i("--Logout", "success");

                    app.setProfileModel(new ProfileModel());
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mView.onSuccessLogout();
                        }
                    });
                }
            }

            @Override
            public void onError(Throwable e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mView.onErrorLogout();
                    }
                });
            }

            @Override
            public void onNext(ResponseObject result) {
                mResponseObject = result;
            }
        });
    }


    interface ProfileService {
        @GET("/staging/registration/webservices/PartnerPlusRegistration.asmx/GetCRMForMobile")
        Observable<ResponseObject> getProfile(@Query("mid") String username);

        @GET("/staging/registration/webservices/PartnerPlusRegistration.asmx/UpdateCRMFromMobile")
        Observable<ResponseObject> sendProfileToCRM(@Query("mid") String membershipid, @Query("cid") String contactid, @Query("aid") String contactaccountid, @Query("cad") String address, @Query("city") String city, @Query("jt") String jobtitle, @Query("jf") String jobfocus, @Query("bt") String businesstype, @Query("js") String jobsites, @Query("aci") String customerindustries, @Query("cmg") String jobfunction, @Query("cjs") String jobspecificrole, @Query("aS") String serviceprovidedbycompany, @Query("cyx") String yearsofexp, @Query("cl") String preferredlanguage, @Query("cmt") String mobiletype, @Query("chd") String mobiledata, @Query("cka") String keyarea);

        @GET("/staging/membershipmanager/MembershipManager.ashx?transaction=5")
        Observable<ResponseObject> submitMembershipIfLoginOrLogout(@Query("MembershipID") String membershipID, @Query("isLogged") boolean isLogged);

    }

}
