package emerson.com.zxservicefield.home.profile;

import android.app.Activity;
import android.content.SharedPreferences;

import emerson.com.zxservicefield.data.response.ProfileModel;

/**
 * Created by mobileapps on 10/28/16.
 */

public interface ProfileContract {
    interface View{
        void onSuccessUpdateProfile();
        void onFailureUpdateProfile(String errorMessage);
        void onSuccessLogout();
        void onErrorLogout();
    }
    interface Presenter{
        void saveProfile(SharedPreferences sharedPreferences, ProfileModel profile);
        void updateProfile(Activity activity, ProfileModel profile);
        void logout(Activity activity);
    }
}
