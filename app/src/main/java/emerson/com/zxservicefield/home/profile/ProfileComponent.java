package emerson.com.zxservicefield.home.profile;

import dagger.Component;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 10/28/16.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = ProfileModule.class)
public interface ProfileComponent {
    void inject(ProfileFragment fragment);
}