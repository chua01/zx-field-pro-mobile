package emerson.com.zxservicefield.home.installation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.HomeToolbar;
import emerson.com.zxservicefield.util.AppUtils;

/**
 * Created by mobileapps on 10/28/16.
 */

public class InstallationFragment extends Fragment implements HomeToolbar, InstallationContract.View{

    private HomeActivity mHomeActivity;
    private View mView;
    private AppUtils mUtils;
    private SweetAlertDialog mDialog;

    @Inject
    InstallationPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.frament_installation,container,false);

        Button btnSend = (Button) mView.findViewById(R.id.send);
        btnSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mUtils = new AppUtils(getActivity());

                mDialog = mUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), "Sending analytics.");
                mDialog.show();

                mPresenter.send(getActivity());

            }
        });

        mHomeActivity = ((HomeActivity)getActivity());
        mHomeActivity.setToolbar(this);
        mHomeActivity.setLeftButtonVisibility(View.GONE);
        mHomeActivity.setRightButtonVisibility(View.GONE);
        mHomeActivity.setTitle(getResources().getString(R.string.title_installation));

        DaggerInstallationComponent.builder()
                .netComponent(((App) getActivity().getApplication()).getNetComponent())
                .installationModule(new InstallationModule(this))
                .build().inject(this);


        return mView;

    }

    @Override
    public void onLeftButtonClicked() {

    }

    @Override
    public void onRightButtonClicked() {

    }

    @Override
    public void onSuccess() {
        if (mDialog.isShowing()){
            mDialog.dismissWithAnimation();
        }
    }

    @Override
    public void onFailed() {
        if (mDialog.isShowing()){
            mDialog.dismissWithAnimation();
        }
    }
}
