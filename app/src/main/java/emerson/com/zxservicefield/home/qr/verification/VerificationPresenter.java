package emerson.com.zxservicefield.home.qr.verification;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.inject.Inject;

import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.data.response.ResponseObject;
import emerson.com.zxservicefield.data.response.UnitModel;
import retrofit2.Retrofit;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mobileapps on 11/7/16.
 */

public class VerificationPresenter implements VerificationContract.Presenter{

    private static final String TAG = VerificationPresenter.class.getSimpleName();

    private Retrofit mRetrofit;
    private VerificationContract.View mView;
    private App mApp;
    private ProfileModel mProfileModel;
    private ResponseObject mResponseObject;
    private String mError;
    private UnitModel mUnitModel;

    @Inject
    public VerificationPresenter(Retrofit retrofit, VerificationContract.View view) {
        this.mRetrofit = retrofit;
        this.mView = view;
    }

    @Override
    public void verify(final Activity activity, UnitModel unitModel) {
        mApp = ((App) activity.getApplication());
        mProfileModel = mApp.getProfileModel();
        Observable<ResponseObject> call = mRetrofit.create(VerificationService.class).verify(
                unitModel.getCustomerName(),
                unitModel.getModelNumber(),
                unitModel.getSerialNumber(),
                unitModel.getInstallationCompany(),
                "0",
                mProfileModel.getA_Name(),
                unitModel.getLocation(),
                unitModel.getLocationLat(),
                unitModel.getLocationLong(),
                mProfileModel.getID(),
                mProfileModel.getC_FirstName()+" "+mProfileModel.getC_LastName(),
                unitModel.getCountry()
        );
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<ResponseObject>() {
                    @Override
                    public void onCompleted() {
                        mError = mResponseObject.getErrorMessage();
                        Log.i("*** onCompleted ***", "error: " + mResponseObject.getErrorMessage());
                        Log.i("*** onCompleted ***", "response item : " + mResponseObject.getResponseItem());
                        if (!TextUtils.isEmpty(mError)) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mView.onFailed();
                                }
                            });
                        }else {
                            Gson gson = new Gson();
                            mUnitModel = gson.fromJson(gson.toJson(mResponseObject.getResponseItem()), new TypeToken<UnitModel>() {}.getType());
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mView.onSuccess(mUnitModel);
                                }
                            });

                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.onFailed();
                            }
                        });
                    }

                    @Override
                    public void onNext(ResponseObject result) {
                        Log.i("*** onNext ***", "next");
                        mResponseObject = result;
                    }
                });



    }

    interface VerificationService{
        @FormUrlEncoded
        @POST("http://phectsmw-dev01.emrsn.org:9322/GenericHandler.ashx?transact=ValidateGetHistory")
        Observable<ResponseObject> verify(
                @Field("customerName") String customerName,
                @Field("modelNumber") String modelNumber,
                @Field("serialNumber") String serialNumber,
                @Field("endUser") String endUser,
                @Field("applicationTypeID") String applicationTypeID,
                @Field("installationCompany") String installationCompany,
                @Field("location") String location,
                @Field("locationLat") double locationLat,
                @Field("locationLong") double locationLong,
                @Field("membershipID") String membershipID,
                @Field("memberName") String memberName,
                @Field("country") String country
        );
    }
}
