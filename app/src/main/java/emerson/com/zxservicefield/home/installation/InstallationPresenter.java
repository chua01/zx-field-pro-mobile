package emerson.com.zxservicefield.home.installation;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;

import javax.inject.Inject;

import emerson.com.zxservicefield.data.response.UploadResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mobileapps on 11/9/16.
 */

public class InstallationPresenter implements InstallationContract.Presenter {

    private String TAG = InstallationPresenter.class.getSimpleName();

    private Retrofit mRetrofit;
    private InstallationContract.View mView;
    private UploadResponse uploadResponse;

    @Inject
    public InstallationPresenter(Retrofit retrofit, InstallationContract.View view) {
        this.mRetrofit = retrofit;
        this.mView = view;
    }

    @Override
    public void send(Activity activity) {
        JSONArray data = new JSONArray();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ID", 1);
            jsonObject.put("Activity", "Android_Test");
            jsonObject.put("ActivityLogID", 1);
            jsonObject.put("ActivityType", 1);
            jsonObject.put("ProductRegistrationID", 1);
            jsonObject.put("DateStart", "01/31/1991");
            jsonObject.put("DateEnd", "01/31/1991");
            jsonObject.put("TimeSpent", 1);
            jsonObject.put("Points", 1);
            jsonObject.put("StepID", 1);
            data.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        File file_training = generateFile(activity, "items.txt", data.toString());
        uploadFile(file_training);


    }

    private void uploadFile(File file) {

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("userfile", file.getName(), requestFile);


        // finally, execute the request
        Observable<UploadResponse> call = mRetrofit.create(InstallationService.class).sync(body);
        call.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<UploadResponse>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "" + uploadResponse.getResult());
                Log.d(TAG, "" + uploadResponse.getMessage());
                mView.onSuccess();
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "" + e.getMessage());
                mView.onFailed();
            }

            @Override
            public void onNext(UploadResponse response) {
                uploadResponse = response;
            }


        });
    }

    public File generateFile(Context context, String sFileName, String sBody) {

        File file = new File(context.getFilesDir(), sFileName);

        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(file, false);
            fOut.write(sBody.getBytes());
            fOut.close();

        } catch (Exception e) {

        }
        return file;

    }


    interface InstallationService {

        @Multipart
        @POST("http://phectsmw-dev01.emrsn.org:9322/UploadAnalytics.ashx")
        Observable<UploadResponse> sync(@Part MultipartBody.Part file);
    }
}
