package emerson.com.zxservicefield.home.qr.application;

import dagger.Component;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/10/16.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(ApplicationAdapter adapter);
}
