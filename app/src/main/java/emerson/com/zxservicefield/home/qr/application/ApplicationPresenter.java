package emerson.com.zxservicefield.home.qr.application;

import javax.inject.Inject;

import emerson.com.zxservicefield.data.response.ResponseObject;
import retrofit2.Retrofit;
import retrofit2.http.Field;
import retrofit2.http.GET;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mobileapps on 11/10/16.
 */

public class ApplicationPresenter implements ApplicationContract.Presenter {

    private Retrofit mRetrofit;
    private ApplicationContract.View mView;
    private ResponseObject mResponseObject;

    @Inject
    public ApplicationPresenter(Retrofit retrofit, ApplicationContract.View view) {
        this.mRetrofit = retrofit;
        this.mView = view;
    }

    @Override
    public void setApplicationType(String modelNum, String serialNumber, String applicationTypeID) {
        Observable<ResponseObject> call = mRetrofit.create(ApplicationService.class).setApplicationType(modelNum, serialNumber, applicationTypeID);
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<ResponseObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ResponseObject responseObject) {
                        mResponseObject = responseObject;
                    }
                });
    }

    interface ApplicationService {
        @GET("http://phectsmw-dev01.emrsn.org:9322/GenericHandler.ashx?transact=UpdateApplicationType")
        Observable<ResponseObject> setApplicationType(
                @Field("modelNum") String modelNum,
                @Field("serialNumber") String serialNumber,
                @Field("applicationTypeID") String applicationTypeID);
    }

}
