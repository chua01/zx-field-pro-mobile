package emerson.com.zxservicefield.home.installation;

import dagger.Component;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/9/16.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = InstallationModule.class)
public interface InstallationComponent {
    void inject(InstallationFragment activity);
}