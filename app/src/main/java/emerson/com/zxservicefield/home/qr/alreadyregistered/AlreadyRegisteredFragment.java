package emerson.com.zxservicefield.home.qr.alreadyregistered;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.home.HomeActivity;

/**
 * Created by mobileapps on 11/8/16.
 */

public class AlreadyRegisteredFragment extends Fragment{

    private HomeActivity mHomeActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registered,container,false);
        mHomeActivity = ((HomeActivity)getActivity());
        mHomeActivity.setTabBarVisibility(View.VISIBLE);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }






}
