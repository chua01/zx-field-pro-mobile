package emerson.com.zxservicefield.home.installation;

import dagger.Module;
import dagger.Provides;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/9/16.
 */

@Module
public class InstallationModule {
    private final InstallationContract.View mView;


    public InstallationModule(InstallationContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    InstallationContract.View providesInstallationContractView() {
        return mView;
    }
}
