package emerson.com.zxservicefield.home.installation;

import android.app.Activity;

/**
 * Created by mobileapps on 11/9/16.
 */

public interface InstallationContract {

    interface View{
        void onSuccess();
        void onFailed();
    }

    interface Presenter{
        void send(Activity activity);
    }

}
