package emerson.com.zxservicefield.home;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.ProfileCRMModel;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.data.response.ResponseObject;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mobileapps on 11/2/16.
 */

public class HomePresenter implements HomeContract.Presenter {

        private ResponseObject mResponseObject;
        private HomeContract.View mView;
        private Retrofit mRetrofit;
        private App mApp;
        private ProfileCRMModel profileCRMModel;
        private String mError;

        @Inject
        public HomePresenter(Retrofit retrofit, HomeContract.View view) {
            this.mRetrofit = retrofit;
            this.mView = view;
        }

        @Override
        public void getUser(final Activity activity, ProfileModel profileModel) {
            mApp = ((App) activity.getApplication());
            mResponseObject = new ResponseObject();
            Observable<ResponseObject> call = mRetrofit.create(HomeService.class).getProfile(mApp.getProfileModel().getID());
            call.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Observer<ResponseObject>() {
                        @Override
                        public void onCompleted() {
                            mError = mResponseObject.getErrorMessage();
                            Log.i("*** onCompleted ***", "error: " + mResponseObject.getErrorMessage());
                            if (!TextUtils.isEmpty(mError)) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mView.onFailure(mError);
                                    }
                                });
                            }else {
                                Gson gson = new Gson();
                                profileCRMModel = gson.fromJson(gson.toJson(mResponseObject.getResponseItem()), new TypeToken<ProfileCRMModel>() {}.getType());
                                ProfileModel profileModel = ((App) activity.getApplication()).getProfileModel();
                                profileModel.setC_LastName(profileCRMModel.getContactLastName());
                                profileModel.setC_FirstName(profileCRMModel.getContactFirstName());
                                profileModel.setA_Address(profileCRMModel.getContactAddress1());
                                profileModel.setA_Name(profileCRMModel.getBusinessName());
                                profileModel.setC_Phone(profileCRMModel.getContactCellularPhone());
                                profileModel.setA_Country(profileCRMModel.getContactCountry());
                                profileModel.setU_EmailAddress(profileCRMModel.getContactEmailAddress());
                                profileModel.setA_City(profileCRMModel.getContactCity());
                                profileModel.setJobTitle(profileCRMModel.getContactJobTitle());
                                profileModel.setContactAccountId(profileCRMModel.getContactAccountId());
                                profileModel.setContactId(profileCRMModel.getContactId());
                                profileModel.setAccountType(profileCRMModel.getAccountType());

                                profileModel.setSelection1(profileCRMModel.getContactPrimaryJobFocus());
                                profileModel.setSelection2(profileCRMModel.getContactType().toString());
                                profileModel.setSelection3(new ArrayList<String>(Arrays.asList((profileCRMModel.getContactListOfCustomerIndustry()).split(","))));
                                profileModel.setSelection4(profileCRMModel.getJobFunction());
                                profileModel.setSelection5(profileCRMModel.getJobSpecificRole());
                                profileModel.setSelection6(new ArrayList<String>(Arrays.asList((profileCRMModel.getContactListOfCompanyService()).split(","))));
                                profileModel.setSelection7(profileCRMModel.getContactYearsOfHVACExp());
                                profileModel.setSelection8(profileCRMModel.getContactLanguage());
                                profileModel.setSelection9(profileCRMModel.getContactMobileDevice());
                                profileModel.setSelection10(profileCRMModel.getContactHasMobileAccessOutsideOffice());
                                profileModel.setSelection11(new ArrayList<String>(Arrays.asList((profileCRMModel.getContactListOfKeyAreasToImprove().trim()).split(","))));

                                ((App) activity.getApplication()).setProfileModel(profileModel);

                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mView.onSuccess();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mView.onFailure(activity.getResources().getString(R.string.error_no_internet));
                                }
                            });
                        }

                        @Override
                        public void onNext(ResponseObject result) {
                            Log.i("*** onNext ***", "next");
                            mResponseObject = result;
                        }
                    });
        }

        interface HomeService {
            @GET("/staging/registration/webservices/PartnerPlusRegistration.asmx/GetCRMForMobile")
            Observable<ResponseObject> getProfile(@Query("mid") String username);
        }
}
