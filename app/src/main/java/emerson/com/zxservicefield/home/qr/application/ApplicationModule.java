package emerson.com.zxservicefield.home.qr.application;

import dagger.Module;
import dagger.Provides;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/10/16.
 */

@Module
public class ApplicationModule {
    private final ApplicationContract.View mView;


    public ApplicationModule(ApplicationContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    ApplicationContract.View providesApplicationContractView() {
        return mView;
    }
}
