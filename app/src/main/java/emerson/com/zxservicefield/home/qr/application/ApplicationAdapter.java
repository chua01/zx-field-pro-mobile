package emerson.com.zxservicefield.home.qr.application;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.UnitModel;
import emerson.com.zxservicefield.home.qr.detail.ApplicationDetailFragment;

/**
 * Created by mobileapps on 11/8/16.
 */

public class ApplicationAdapter extends RecyclerView.Adapter<ApplicationAdapter.ViewHolder> implements ApplicationContract.View{


    private Context mContext;
    private ArrayList<String> mList;
    private UnitModel mUnitModel;

    @Inject
    ApplicationPresenter mPresenter;

    public ApplicationAdapter(Context mContext, ArrayList<String> application) {
        this.mContext = mContext;
        this.mList = application;
    }

    public void setUnitModel(UnitModel mUnitModel) {
        this.mUnitModel = mUnitModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_application, parent, false);

        DaggerApplicationComponent.builder()
                .netComponent(((App) ((Activity)mContext).getApplication()).getNetComponent())
                .applicationModule(new ApplicationModule(this))
                .build().inject(this);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(mList.get(position));
        switch (position) {
            case 0:
                holder.mImageView.setImageResource(R.drawable.bloodcenter);
                break;
            case 1:
                holder.mImageView.setImageResource(R.drawable.conviencestore);
                break;
            case 2:
                holder.mImageView.setImageResource(R.drawable.fastfood);
                break;
            case 3:
                holder.mImageView.setImageResource(R.drawable.processfood);
                break;
            case 4:
                holder.mImageView.setImageResource(R.drawable.hotel);
                break;
            case 5:
                holder.mImageView.setImageResource(R.drawable.medical);
                break;
            case 6:
                holder.mImageView.setImageResource(R.drawable.refandfreezer);
                break;
            case 7:
                holder.mImageView.setImageResource(R.drawable.supermarket);
                break;
            case 8:

                break;
        }

        holder.mCardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationDetailFragment applicationDetailFragment = new ApplicationDetailFragment();
                applicationDetailFragment.setUnitModel(mUnitModel);
                pushFragment(applicationDetailFragment, "applicationdetail");
            }
        });
    }

    private void pushFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment, tag);
        ft.addToBackStack(tag);
        ft.commit();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailed() {

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

        @Bind(R.id.thumbnail)
        ImageView mImageView;
        @Bind(R.id.title)
        TextView mTextView;
        @Bind(R.id.card_view_layout)
        RelativeLayout mCardView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            ApplicationDetailFragment applicationDetailFragment = new ApplicationDetailFragment();
            applicationDetailFragment.setUnitModel(mUnitModel);
            pushFragment(applicationDetailFragment, "applicationdetail");
        }
    }


}
