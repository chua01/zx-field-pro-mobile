package emerson.com.zxservicefield.home.qr.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.UnitModel;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.qr.step.StepFragment;

/**
 * Created by mobileapps on 11/8/16.
 */

public class ApplicationDetailFragment extends Fragment {

    private HomeActivity mHomeActivity;
    private UnitModel mUnitModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_application_detail,container,false);
        ButterKnife.bind(this,view);

        mHomeActivity = ((HomeActivity)getActivity());
        mHomeActivity.setTabBarVisibility(View.VISIBLE);
        
        return view;
    }

    public void setUnitModel(UnitModel mUnitModel) {
        this.mUnitModel = mUnitModel;
    }

    @OnClick({R.id.installation_layout,R.id.fieldservice_layout})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.installation_layout:
            case R.id.fieldservice_layout:
                StepFragment stepFragment = new StepFragment();
                stepFragment.setUnitModel(mUnitModel);
                pushFragment(stepFragment,"step");
                break;
        }

    }

    private void pushFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment, tag);
        ft.commit();

    }
}
