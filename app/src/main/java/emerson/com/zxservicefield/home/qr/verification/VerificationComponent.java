package emerson.com.zxservicefield.home.qr.verification;

import dagger.Component;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/7/16.
 */
@CustomScope
@Component(dependencies = NetComponent.class, modules = VerificationModule.class)
public interface VerificationComponent {
    void inject(VerificationFragment fragment);
}