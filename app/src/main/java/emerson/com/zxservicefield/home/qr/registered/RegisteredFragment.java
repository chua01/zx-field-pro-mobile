package emerson.com.zxservicefield.home.qr.registered;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.UnitModel;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.qr.application.ApplicationFragment;

/**
 * Created by mobileapps on 11/8/16.
 */

public class RegisteredFragment extends Fragment {

    private HomeActivity mHomeActivity;
    private UnitModel mUnitModel;

    public void setUnitModel(UnitModel mUnitModel) {
        this.mUnitModel = mUnitModel;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_registered,container,false);
        ButterKnife.bind(this,view);
        mHomeActivity = ((HomeActivity)getActivity());
        mHomeActivity.setTabBarVisibility(View.GONE);
        return view;
    }

    @OnClick({R.id.btnOk})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btnOk:
                ApplicationFragment applicationFragment = new ApplicationFragment();
                applicationFragment.setUnitModel(mUnitModel);
                pushFragment(applicationFragment,"application");
                break;
        }
    }

    private void pushFragment(Fragment fragment, String tag){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout,fragment,tag);
        ft.commit();
    }





}
