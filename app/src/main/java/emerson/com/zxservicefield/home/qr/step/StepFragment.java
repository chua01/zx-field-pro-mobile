package emerson.com.zxservicefield.home.qr.step;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.StepContentModel;
import emerson.com.zxservicefield.data.response.StepFieldModel;
import emerson.com.zxservicefield.data.response.StepModel;
import emerson.com.zxservicefield.data.response.UnitModel;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.HomeToolbar;
import emerson.com.zxservicefield.util.AppUtils;

/**
 * Created by mobileapps on 11/11/16.
 */

public class StepFragment extends Fragment implements HomeToolbar {

    @Bind(R.id.stepLayout)
    LinearLayout mStepLayout;


    private StepModel mStepModel;
    private ArrayList<StepContentModel> mStepContentModel;
    private ArrayList<StepFieldModel> mStepFieldModel;
    private AppUtils mAppUtils;
    private App mApp;
    private HomeActivity mHomeActivity;
    private ArrayList<Integer> mSteps;
    private View mView;
    private SweetAlertDialog sweetAlertDialog;
    private UnitModel mUnitModel;
    private RadioGroup mRadioGroup;

    public void setUnitModel(UnitModel mUnitModel) {
        this.mUnitModel = mUnitModel;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_step, container, false);
        ButterKnife.bind(this, mView);

        mSteps = new ArrayList<>();

        mApp = (App) getActivity().getApplication();

        mHomeActivity = ((HomeActivity) getActivity());
        mHomeActivity.setToolbar(this);
        mHomeActivity.setRightButtonVisibility(View.VISIBLE);
        mHomeActivity.setRightButton(R.drawable.ic_next);

        mStepModel = mApp.getDbHandler().getStep(1);
        mStepContentModel = mApp.getDbHandler().getStepContent(mStepModel.getContentID());
        mStepFieldModel = mApp.getDbHandler().getStepField(mStepModel.getStepID());

        mAppUtils = new AppUtils(getActivity());

        initializeUI();

        return mView;
    }

    private void initializeUI() {
        mStepLayout.removeAllViews();

        TextView tvStepTitle = new TextView(getActivity());
        tvStepTitle.setText(mStepModel.getStepName());
        mStepLayout.addView(tvStepTitle);

        TextView tvReplacementPart = new TextView(getActivity());

        tvReplacementPart.setText(mStepModel.getMainText());
        mStepLayout.addView(tvReplacementPart);

        ImageView ivImage = new ImageView(getActivity());
        ivImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        mAppUtils.cacheImage(ivImage, "https://i-cdn.phonearena.com/images/articles/47012-image/photo2.jpg");
        mStepLayout.addView(ivImage);

        for (StepContentModel model : mStepContentModel) {
//            if (model.getImageFile() != null) {
//                ImageView ivImage = new ImageView(getActivity());
//                mAppUtils.cacheImage(ivImage, model.getImageFile());
//                mStepLayout.addView(ivImage);
//            }


//            if (model.getVideoFile() != null) {
//                ImageView ivImage = new ImageView(getActivity());
//                mAppUtils.cacheImage(ivImage, model.getImageFile());
//                mStepLayout.addView(ivImage);
//            }

            if (!TextUtils.isEmpty(model.getReplacementPart())) {
                String part = mApp.getDbHandler().getPart(model.getReplacementPart(), mUnitModel.getModelNumber());
                tvReplacementPart.setText(mStepModel.getMainText() + " Replaced part with " + part + ".");
            }

        }

        if (mStepFieldModel.size() > 1) {
            mRadioGroup = new RadioGroup(getActivity());
            for (StepFieldModel model : mStepFieldModel) {
                RadioButton radioButton = new RadioButton(getActivity());
                radioButton.setText(model.getSTEPFIELD_VALUE());
                mRadioGroup.addView(radioButton);
            }
            mStepLayout.addView(mRadioGroup);
        }
    }


    private void pushFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment, tag);
        ft.addToBackStack(tag);
        ft.commit();
    }

    public void getStep(UnitModel unitModel) {

    }

    public void getStep(int code) {

    }

    private void setProgressDialog() {
        sweetAlertDialog = mAppUtils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_register_account));
    }

    private void setErrorMessage(String error) {
        setProgressDialog();
        sweetAlertDialog.setTitleText("Message")
                .setContentText(error)
                .setConfirmText(getResources().getString(R.string.dialog_ok))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmClickListener(null)
                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.show();
    }

    @Override
    public void onLeftButtonClicked() {
        if (mSteps.size() == 1) {
            mHomeActivity.setLeftButtonVisibility(View.GONE);
        }
        if (mSteps.size() > 0) {
            int previousID = mSteps.get(mSteps.size() - 1);
            mSteps.remove(mSteps.size() - 1);
            mStepModel = mApp.getDbHandler().getStep(previousID);
            mStepContentModel = mApp.getDbHandler().getStepContent(mStepModel.getContentID());
            mStepFieldModel = mApp.getDbHandler().getStepField(mStepModel.getStepID());
        }

        initializeUI();

    }

    @Override
    public void onRightButtonClicked() {
        int nextID = 0;

        if (mStepFieldModel.size() > 1) {
            int selectedId = mRadioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = (RadioButton) mView.findViewById(selectedId);
            if (radioButton != null) {
                String value = radioButton.getText().toString();
                for (StepFieldModel model : mStepFieldModel) {
                    if (value.equals(model.getSTEPFIELD_VALUE())) {
                        nextID = model.getNEXTSTEP_ID();
                    }
                }
            } else {
                setErrorMessage("Please answer all the questions.");
            }
        } else if (mStepFieldModel.size() == 1) {
            nextID = mStepFieldModel.get(0).getNEXTSTEP_ID();
        }

        if (nextID != 0) {
            mSteps.add(mStepModel.getStepID());

            mStepModel = mApp.getDbHandler().getStep(nextID);
            mStepContentModel = mApp.getDbHandler().getStepContent(mStepModel.getStepID());
            mStepFieldModel = mApp.getDbHandler().getStepField(mStepModel.getStepID());

            initializeUI();
            mHomeActivity.setLeftButtonVisibility(View.VISIBLE);
            mHomeActivity.setLeftButton(R.drawable.ic_back);
        }


    }
}
