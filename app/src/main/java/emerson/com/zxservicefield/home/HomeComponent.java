package emerson.com.zxservicefield.home;

import dagger.Component;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/2/16.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = HomeModule.class)
public interface HomeComponent {
    void inject(HomeActivity activity);
}