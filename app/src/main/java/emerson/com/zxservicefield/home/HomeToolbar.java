package emerson.com.zxservicefield.home;

/**
 * Created by mobileapps on 10/28/16.
 */

public interface HomeToolbar {

    void onLeftButtonClicked();
    void onRightButtonClicked();

}
