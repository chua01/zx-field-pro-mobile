package emerson.com.zxservicefield.home;

import dagger.Module;
import dagger.Provides;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 11/2/16.
 */

@Module
public class HomeModule {
    private final HomeContract.View mView;


    public HomeModule(HomeContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    HomeContract.View providesHomeContractView() {
        return mView;
    }
}