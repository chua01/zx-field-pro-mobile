package emerson.com.zxservicefield.customviews;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.home.card.CardFragment;
import emerson.com.zxservicefield.home.profile.ProfileFragment;
import emerson.com.zxservicefield.home.qr.QRScanFragment;

/**
 * Created by mobileapps on 10/26/16.
 */

public class TabBarView extends RelativeLayout implements View.OnClickListener{

    private Context mContext;
    private Fragment activeFragment;
    private ImageView ivProfile;
    private ImageView ivCard;
    private ImageView ivQr;


    public Fragment getActiveFragment() {
        return activeFragment;
    }

    public TabBarView(Context context) {
        super(context);
        initializeViews(context);
    }

    public TabBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public TabBarView(Context context,
                       AttributeSet attrs,
                       int defStyle) {
        super(context, attrs, defStyle);
        initializeViews(context);
    }

    /**
     * Inflates the views in the layout.
     *
     * @param context
     *           the current context for the view.
     */
    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        View view = inflater.inflate(R.layout.content_tab_bar, this);
        LinearLayout ll_profile = (LinearLayout) view.findViewById(R.id.tab_profile);
        LinearLayout ll_card_ = (LinearLayout) view.findViewById(R.id.tab_card);
        LinearLayout ll_scanqr = (LinearLayout) view.findViewById(R.id.tab_scanqr);

        ivProfile = (ImageView) view.findViewById(R.id.img_tab_profile);
        ivCard = (ImageView) view.findViewById(R.id.img_tab_card);
        ivQr = (ImageView) view.findViewById(R.id.img_tab_qr);


        ll_profile.setOnClickListener(this);
        ll_card_.setOnClickListener(this);
        ll_scanqr.setOnClickListener(this);

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tab_profile:
                ivProfile.setImageResource(R.drawable.profile_active);
                ivCard.setImageResource(R.drawable.ic_tab_card);
                ivQr.setImageResource(R.drawable.ic_tab_qr);
                activeFragment = new ProfileFragment();
                replaceFragment(activeFragment);
                break;
            case R.id.tab_card:
                ivProfile.setImageResource(R.drawable.ic_tab_profile);
                ivCard.setImageResource(R.drawable.card_active);
                ivQr.setImageResource(R.drawable.ic_tab_qr);
                activeFragment = new CardFragment();
                replaceFragment(activeFragment);
                break;
            case R.id.tab_scanqr:
                ivQr.setImageResource(R.drawable.qr_active);
                ivProfile.setImageResource(R.drawable.ic_tab_profile);
                ivCard.setImageResource(R.drawable.ic_tab_card);
                activeFragment = new QRScanFragment();
                replaceFragment(activeFragment);
                break;
        }
    }

    private void replaceFragment(Fragment fragment){
        FragmentTransaction ft = ((HomeActivity)mContext).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.commit();
    }
}
