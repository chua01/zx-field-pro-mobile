package emerson.com.zxservicefield;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mobileapps on 10/24/16.
 */

@Module
public class AppModule {
    App mApplication;

    public AppModule(App mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @Singleton
    App provideApplication() {
        return mApplication;
    }
}
