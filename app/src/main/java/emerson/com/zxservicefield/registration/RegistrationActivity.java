package emerson.com.zxservicefield.registration;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.adapter.CustomAlertAdapter;
import emerson.com.zxservicefield.data.response.Countries;
import emerson.com.zxservicefield.data.response.RegistrationModel;
import emerson.com.zxservicefield.util.AppUtils;
import emerson.com.zxservicefield.util.RecyclerViewTouchListener;

/**
 * Created by mobileapps on 10/24/16.
 */

public class RegistrationActivity extends Activity implements RegistrationContract.View {

    @Bind(R.id.et_last_name)
    EditText etLastName;
    @Bind(R.id.et_first_name)
    EditText etFirstName;
    @Bind(R.id.et_company_name)
    EditText etCompanyName;
    @Bind(R.id.tv_city)
    TextView tvCity;
    @Bind(R.id.tv_country)
    TextView tvCountry;
    @Bind(R.id.tv_dial_code)
    TextView tvDialCode;
    @Bind(R.id.et_mobile)
    EditText etMobile;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.ivBack)
    ImageView ivBack;
    @Bind(R.id.ivDone)
    ImageView ivDone;

    @Inject
    RegistrationPresenter mPresenter;

    private View customAlertView;
    private View view;
    private App app;
    private Gson gson;
    private ArrayList<Countries> countries;
    private ArrayList<String> cities;
    private AlertDialog dialog = null;
    private SweetAlertDialog sweetAlertDialog;
    private String country_code;
    private ArrayList<String> array_sort;
    int textlength = 0;
    private AppUtils utils;
    private SharedPreferences sharedPreferences;
    private String blockCharacterSet = "~`!@#$%^*_+=:\\\";<>?/{}()&|[]\\\\0123456789";
    private String blockCharacterSet2 = "`~`!@#$%^*_+=:\\\";<>?/{}()&|[]-\\\\";
    private List<String> temporary_countries = Arrays.asList("Australia", "New Zealand", "China", "India", "Indonesia", "Taiwan", "Japan", "Malaysia", "Philippines", "Singapore", "South Korea", "Thailand", "Vietnam", "Hong Kong", "Algeria", "Angola", "Bahrain", "Benin", "Botswana", "Burkina Faso", "Cameroon", "Chad", "Democratic Republic of the Congo", "Egypt", "Equatorial Guinea", "Eritrea", "Ethiopia", "Gabon", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Iraq", "Israel", "Ivory Coast", "Jordan", "Kenya", "Kuwait", "Lebanon", "Liberia", "Libya", "Madagascar", "Malawi", "Mali", "Mauritania", "Mauritius", "Morocco", "Mozambique", "Namibia", "Niger", "Nigeria", "Oman", "Qatar", "Pakistan", "Rwanda", "Saudi Arabia", "Senegal", "South Africa", "Swaziland", "Tanzania", "Togo", "Tunisia", "Uganda", "United Arab Emirates", "Zambia", "Zimbabwe");
    private List<String> mea_countries = Arrays.asList("Algeria", "Angola", "Bahrain", "Benin", "Botswana", "Burkina Faso", "Cameroon", "Chad", "Democratic Republic of the Congo", "Egypt", "Equatorial Guinea", "Eritrea", "Ethiopia", "Gabon", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Iraq", "Ivory Coast", "Jordan", "Kenya", "Kuwait", "Lebanon", "Liberia", "Libya", "Madagascar", "Malawi", "Mali", "Mauritania", "Mauritius", "Morocco", "Mozambique", "Namibia", "Niger", "Nigeria", "Oman", "Qatar", "Rwanda", "Saudi Arabia", "Senegal", "South Africa", "Swaziland", "Tanzania", "Togo", "Tunisia", "Uganda", "United Arab Emirates", "Zambia", "Zimbabwe", "Israel", "Pakistan");
    private String mBlockCharacterSet = "~`!@#$%^*_+=:\\\";<>?/{}()&|[]\\\\0123456789";
    private InputFilter mNameFilter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && mBlockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        DaggerRegistrationComponent.builder()
                .netComponent(((App) getApplicationContext()).getNetComponent())
                .registrationModule(new RegistrationModule(this))
                .build().inject(this);

        utils = new AppUtils(this);
        gson = new Gson();
        countries = gson.fromJson(utils.readTxt(R.raw.phone), new TypeToken<ArrayList<Countries>>() {
        }.getType());

        resetCitiesArray();
        setProgressDialog();

        etFirstName.setFilters(new InputFilter[]{mNameFilter});
        etLastName.setFilters(new InputFilter[]{mNameFilter});
    }

    @OnClick({R.id.ivBack, R.id.ivDone, R.id.tv_country, R.id.tv_city})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.ivDone:
                onDoneClicked();
                break;
            case R.id.tv_country:
                getCountries();
                break;
            case R.id.tv_city:
                getCities();
                break;
        }
    }

    @Override
    public void showError(String error) {

    }


    @Override
    public void onDoneClicked() {
        setProgressDialog();
        sweetAlertDialog.show();
        String country_code = tvDialCode.getText().toString().replace("+", "");
        if (country_code.length() == 2) {
            country_code = "0" + country_code;
        }
        Log.d("Registration", "" + country_code);
        RegistrationModel registrationModel = new RegistrationModel();
        registrationModel.setFirstName(etFirstName.getText().toString().trim().replaceAll("\\s+", " "));
        registrationModel.setLasName(etLastName.getText().toString().trim().replaceAll("\\s+", " "));
        registrationModel.setCity(tvCity.getText().toString().trim().replaceAll("\\s+", " "));
        registrationModel.setContact(etMobile.getText().toString());
        registrationModel.setCountryCode(country_code);
        registrationModel.setCountryName(tvCountry.getText().toString());
        registrationModel.setBusineName(etCompanyName.getText().toString().trim().replaceAll("\\s+", " "));
        registrationModel.setEmail(etEmail.getText().toString().trim().replaceAll("\\s+", " "));
        registrationModel.setAnnouncements("");
        if (mea_countries.contains(tvCountry.getText().toString())) {
            mPresenter.registerMea(this, registrationModel);
        } else {
            mPresenter.register(this, registrationModel);
        }


    }

    @Override
    public void onBackClicked() {
        onBackPressed();
    }

    private void setProgressDialog() {
        sweetAlertDialog = utils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_register_account));
    }


    public void getCountries() {
        final ArrayList<String> items = new ArrayList<>();
        array_sort = new ArrayList<>();
        Collections.sort(temporary_countries);
        array_sort.addAll(temporary_countries);
        final int[] selected = {-1};


        customAlertView = LayoutInflater.from(RegistrationActivity.this).inflate(R.layout.content_custom_alert, null);

        final RecyclerView rvCustomAlertMenu = (RecyclerView) customAlertView.findViewById(R.id.rv_custom_alert_menu);
        rvCustomAlertMenu.setAdapter(new CustomAlertAdapter(RegistrationActivity.this, array_sort));
        rvCustomAlertMenu.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(RegistrationActivity.this);
        rvCustomAlertMenu.setLayoutManager(llm);
        rvCustomAlertMenu.addOnItemTouchListener(new RecyclerViewTouchListener(RegistrationActivity.this, rvCustomAlertMenu, new RecyclerViewTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                for (Countries country : countries) {
                    if (country.getName().equals(array_sort.get(position))) {
                        Countries selectedCountry = country;
                        tvCountry.setText(selectedCountry.getName());
                        tvDialCode.setText(selectedCountry.getDial_code());
                        country_code = selectedCountry.getCode();
                        tvCity.setText("");
                        dialog.dismiss();
                    }
                }
//                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        final EditText etCustomalert = (EditText) customAlertView.findViewById(R.id.et_customalert);
        etCustomalert.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etCustomalert.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                textlength = etCustomalert.getText().length();
                array_sort.clear();
                for (int count = 0; count < temporary_countries.size(); count++) {
                    if (textlength <= temporary_countries.get(count).length()) {

                        if (temporary_countries.get(count).toLowerCase().contains(etCustomalert.getText().toString().toLowerCase().trim())) {
                            array_sort.add(temporary_countries.get(count));
                        }
                    }
                }
                rvCustomAlertMenu.setAdapter(new CustomAlertAdapter(RegistrationActivity.this, array_sort));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle(getResources().getString(R.string.dialog_select_country));
        builder.setView(customAlertView);

        builder.setNeutralButton(getResources().getString(R.string.dialog_dismiss), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnPositive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                Button btnNegative = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);

                btnPositive.setEnabled(false);
            }
        });

        dialog.show();
    }

    public void getCities() {

        resetCitiesArray();

        final ArrayList<String> items = new ArrayList<>();
        array_sort = new ArrayList<>();
        final int[] selected = {-1};

        for (int i = 0; i < cities.size(); i++) {
            items.add(i, cities.get(i));

        }
        array_sort.addAll(items);

        customAlertView = LayoutInflater.from(RegistrationActivity.this).inflate(R.layout.content_custom_alert, null);

        final RecyclerView rvCustomAlertMenu = (RecyclerView) customAlertView.findViewById(R.id.rv_custom_alert_menu);
        rvCustomAlertMenu.setAdapter(new CustomAlertAdapter(RegistrationActivity.this, array_sort));
        rvCustomAlertMenu.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(RegistrationActivity.this);
        rvCustomAlertMenu.setLayoutManager(llm);
        rvCustomAlertMenu.addOnItemTouchListener(new RecyclerViewTouchListener(RegistrationActivity.this, rvCustomAlertMenu, new RecyclerViewTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                tvCity.setText(array_sort.get(position));
                dialog.dismiss();

                tvCity.setEnabled(true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        final EditText etCustomalert = (EditText) customAlertView.findViewById(R.id.et_customalert);
        etCustomalert.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etCustomalert.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                textlength = etCustomalert.getText().length();
                array_sort.clear();
                for (int count = 0; count < items.size(); count++) {
                    if (textlength <= items.get(count).length()) {

                        if (items.get(count).toLowerCase().contains(etCustomalert.getText().toString().toLowerCase().trim())) {
                            array_sort.add(items.get(count));
                        }
                    }
                }
                rvCustomAlertMenu.setAdapter(new CustomAlertAdapter(RegistrationActivity.this, array_sort));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle(getResources().getString(R.string.dialog_select_city));
//
        builder.setView(customAlertView);


        builder.setNeutralButton(getResources().getString(R.string.dialog_dismiss), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();

                tvCity.setEnabled(true);
            }
        });

        dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnPositive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                Button btnNegative = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);

                btnPositive.setEnabled(false);
            }
        });

        dialog.show();


    }

    public void resetCitiesArray() {
        try {
            cities = new ArrayList<>();
            JSONObject jsonObject1 = new JSONObject(utils.readTxt(R.raw.city));
            JSONArray jsonObject2 = jsonObject1.getJSONArray(tvCountry.getText().toString());
            for (int i = 0; i < jsonObject2.length(); i++) {
                cities.add(jsonObject2.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onFailure(String errorMessage, String email) {
        Log.d("TAG", errorMessage);
        if (sweetAlertDialog.isShowing())
            sweetAlertDialog.dismiss();
        if (errorMessage.equals("Registration successful!  A validation e-mail containing your Membership ID and Password has been sent to '" + email + "’.")) {
            if (Locale.getDefault().getDisplayLanguage().equals("한국어")) {
                sweetAlertDialog.setTitleText(getResources().getString(R.string.message))
                        .setContentText("등록이 되었습니다. '" + email + "' 에서 멤버쉽 ID 와 비밀번호를 확인 e-mail로 송부해 드립니다.")
                        .setConfirmText(getResources().getString(R.string.dialog_ok))
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmClickListener(null)
                        .changeAlertType(SweetAlertDialog.NORMAL_TYPE);
            } else {
                sweetAlertDialog.setTitleText(getResources().getString(R.string.message))
                        .setContentText(getResources().getString(R.string.register_success) + " '"+ email + "’.")
                        .setConfirmText(getResources().getString(R.string.dialog_ok))
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmClickListener(null)
                        .changeAlertType(SweetAlertDialog.NORMAL_TYPE);
            }
            sweetAlertDialog.show();
        } else {
            sweetAlertDialog.setTitleText(getResources().getString(R.string.message))
                    .setContentText(errorMessage)
                    .setConfirmText(getResources().getString(R.string.dialog_ok))
                    .showCancelButton(false)
                    .setCancelClickListener(null)
                    .setConfirmClickListener(null)
                    .changeAlertType(SweetAlertDialog.ERROR_TYPE);

            sweetAlertDialog.show();
        }



        etLastName.setText("");
        etFirstName.setText("");
        etCompanyName.setText("");
        tvCity.setText("");
        tvCountry.setText("");
        tvDialCode.setText("");
        etMobile.setText("");
        etEmail.setText("");

    }

    @Override
    public void onSuccess() {
        if (sweetAlertDialog.isShowing())
            sweetAlertDialog.dismiss();
        sweetAlertDialog.setTitleText("Message")
                .setContentText("Success")
                .setConfirmText(getResources().getString(R.string.dialog_ok))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmClickListener(null)
                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.show();
    }

    @Override
    public void setEmptyFieldsError() {

        sweetAlertDialog.setTitleText("Message")
                .setContentText("Empty Fields")
                .setConfirmText(getResources().getString(R.string.dialog_ok))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmClickListener(null)
                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.show();
    }


}
