package emerson.com.zxservicefield.registration;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import javax.inject.Inject;

import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.response.RegistrationModel;
import emerson.com.zxservicefield.data.response.ResponseObject;
import emerson.com.zxservicefield.util.AppUtils;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mobileapps on 10/24/16.
 */

public class RegistrationPresenter implements RegistrationContract.Presenter {

    private RegistrationContract.View mView;
    private Retrofit retrofit;
    private ResponseObject responseObject;
    private AppUtils appUtils;
    private Activity mActivity;

    @Inject
    public RegistrationPresenter(Retrofit retrofit, RegistrationContract.View view) {
        this.retrofit = retrofit;
        mView = view;


    }

    @Override
    public void register(Activity activity, RegistrationModel registration) {
        mActivity = activity;
        appUtils = new AppUtils(activity);
        Log.i("-- registerUser", registration.toString());
        if (TextUtils.isEmpty(registration.getFirstName()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getLasName()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getEmail()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getBusineName()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getCity()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getContact()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getCountryCode()))
            mView.setEmptyFieldsError();
        else if (!appUtils.isEmailValid(registration.getEmail())){
            mView.onFailure(activity.getResources().getString(R.string.error_invalid_email), "");
        }else {
            register(registration);
        }
    }

    private void register(final RegistrationModel registration){
        Observable<ResponseObject> call = retrofit.create(RegisterService.class).submitRegistration(registration.getFirstName(), registration.getLasName(), registration.getEmail(), registration.getContact(), registration.getAnnouncements(), registration.getBusineName(), registration.getCity(), registration.getCountryCode().replaceAll("\\s", ""), registration.getCountryName());
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<ResponseObject>() {

                    @Override
                    public void onCompleted() {
                        String errorMessage = responseObject.getErrorMessage();
                        if (errorMessage == null) {
                            Log.i("*** onResponse ***", "success");
                            mView.onSuccess();
                        } else {
                            Log.i("*** onResponse ***", errorMessage);
                            mView.onFailure(errorMessage,registration.getEmail());
                        }
                    }

                    @Override
                    public void onError(final Throwable e) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.onFailure(e.getMessage(),"");
                            }
                        });
                    }

                    @Override
                    public void onNext(ResponseObject object) {
                        Log.i("*** onResponse ***", object.getErrorMessage());
                        responseObject = object;
                    }
                });
    }

    @Override
    public void registerMea(Activity activity, RegistrationModel registration) {
        mActivity = activity;
        appUtils = new AppUtils(activity);
        Log.i("-- registerUser", registration.toString());
        if (TextUtils.isEmpty(registration.getFirstName()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getLasName()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getEmail()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getBusineName()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getCity()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getContact()))
            mView.setEmptyFieldsError();
        else if (TextUtils.isEmpty(registration.getCountryCode()))
            mView.setEmptyFieldsError();
        else if (!appUtils.isEmailValid(registration.getEmail())){
            mView.onFailure(activity.getResources().getString(R.string.error_invalid_email), "");
        }else {
            registerMea(registration);
        }

    }

    public void registerMea(final RegistrationModel registration){
        Observable<ResponseObject> call = retrofit.create(RegisterService.class).submitRegistrationMEA(registration.getFirstName(), registration.getLasName(), registration.getEmail(), registration.getContact(), registration.getAnnouncements(), registration.getBusineName(), registration.getCity(), registration.getCountryCode().replaceAll("\\s", ""), registration.getCountryName());
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<ResponseObject>() {

                    @Override
                    public void onCompleted() {
                        String errorMessage = responseObject.getErrorMessage();
                        if (errorMessage == null) {
                            Log.i("*** onResponse ***", "success");
                            mView.onSuccess();
                        } else {
                            Log.i("*** onResponse ***", errorMessage);
                            mView.onFailure(errorMessage,registration.getEmail());
                        }
                    }

                    @Override
                    public void onError(final Throwable e) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.onFailure(e.getMessage(),"");
                            }
                        });
                    }

                    @Override
                    public void onNext(ResponseObject object) {
                        Log.i("*** onResponse ***", object.getErrorMessage());
                        responseObject = object;
                    }
                });
    }

    public interface RegisterService {

        @GET("/staging/pws/requesthandler.asmx/SubmitRegistrationFromMobile")
        Observable<ResponseObject> submitRegistration(@Query("c_FirstName") String firstName, @Query("c_LastName") String lastName, @Query("c_Email") String email, @Query("C_Phone") String phone, @Query("a_Announcements") String announcements, @Query("a_Name") String name, @Query("a_City") String address, @Query("c_Country") String countryCode, @Query("a_Country") String countryName);

        @GET("/staging/pwsmea/requesthandler.asmx/SubmitRegistrationFromMobile")
        Observable<ResponseObject> submitRegistrationMEA(@Query("c_FirstName") String firstName, @Query("c_LastName") String lastName, @Query("c_Email") String email, @Query("C_Phone") String phone, @Query("a_Announcements") String announcements, @Query("a_Name") String name, @Query("a_City") String address, @Query("c_Country") String countryCode, @Query("a_Country") String countryName);



    }


}
