package emerson.com.zxservicefield.registration;

import android.app.Activity;

import emerson.com.zxservicefield.data.response.RegistrationModel;


/**
 * Created by mobileapps on 10/24/16.
 */

public interface RegistrationContract {

    interface View {

        void showError(String error);

        void onDoneClicked();

        void onBackClicked();

        void onFailure(String errorMessage, String email);

        void onSuccess();

        void setEmptyFieldsError();

    }

    interface Presenter  {

        void register(Activity activity, RegistrationModel registrationModel);
        void registerMea(Activity activity, RegistrationModel registrationModel);

    }

}
