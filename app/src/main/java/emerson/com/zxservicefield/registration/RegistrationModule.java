package emerson.com.zxservicefield.registration;

import dagger.Module;
import dagger.Provides;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 10/24/16.
 */

@Module
public class RegistrationModule {
    private RegistrationContract.View mView;


    public RegistrationModule(RegistrationContract.View view) {
        this.mView = view;
    }

    @Provides
    @CustomScope
    RegistrationContract.View providesRegistrationContractView() {
        return mView;
    }
}
