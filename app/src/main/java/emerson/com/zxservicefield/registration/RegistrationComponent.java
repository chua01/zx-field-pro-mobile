package emerson.com.zxservicefield.registration;

import dagger.Component;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 10/24/16.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = RegistrationModule.class)
public interface RegistrationComponent {
    void inject(RegistrationActivity activity);
}
