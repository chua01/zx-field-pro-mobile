package emerson.com.zxservicefield.data.source.local;

/**
 * Created by mobileapps on 11/16/16.
 */

public class DBTables {


    public static final String ID = "ID";

    //STEPS

    public static final String STEPS = "STEPS";

    public static final String STEPS_NAME = "STEP_NAME";
    public static final String STEPS_MAIN_TEXT = "MAIN_TEXT";
    public static final String STEPS_CONTENT_ID = "CONTENT_ID";

    //STEP_CONTENT

    public static final String STEPS_CONTENT = "CONTENTS";

    public static final String STEPS_CONTENT_CONTENT_ID = "CONTENT_ID";
    public static final String STEPS_CONTENT_STEP_ID = "STEP_ID";
    public static final String STEPS_CONTENT_IMAGE_FILE = "IMAGE_FILE";
    public static final String STEPS_CONTENT_VIDEO_FILE = "VIDEO_FILE";
    public static final String STEPS_CONTENT_REPLACEMENT_PART = "REPLACEMENT_PART";

    //STEP_FIELDS

    public static final String STEP_FIELDS = "STEP_FIELDS";

    public static final String STEP_FIELDS_STEP_ID = "STEP_ID";
    public static final String STEP_FIELDS_STEPFIELD_VALUE = "STEPFIELD_VALUE";
    public static final String STEP_FIELDS_NEXT_STEP_ID = "NEXTSTEP_ID";

    public static final String PARTS_TABLE = "PARTS_TABLE";

    public static final String PARTS_TABLE_MAIN_MODEL_NUMBER = "MODEL_NUMBER";
    public static final String PARTS_TABLE_MAIN_CONTROLLER_BOARD = "MAINCONTROLLER_BOARD";





}
