package emerson.com.zxservicefield.data.response;


/**
 * Created by mobileapps on 10/17/16.
 */
public class UploadResponse {

    private String Message;
    private String Result;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }
}
