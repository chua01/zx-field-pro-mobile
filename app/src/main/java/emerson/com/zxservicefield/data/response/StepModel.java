package emerson.com.zxservicefield.data.response;

/**
 * Created by mobileapps on 11/14/16.
 */

public class StepModel {

    private int StepID;
    private String StepName;
    private String MainText;
    private int  ActivityTypeID;
    private int GroupID;
    private int ContentID;
    private int LangID;

    public int getStepID() {
        return StepID;
    }

    public void setStepID(int stepID) {
        StepID = stepID;
    }

    public String getStepName() {
        return StepName;
    }

    public void setStepName(String stepName) {
        StepName = stepName;
    }

    public String getMainText() {
        return MainText;
    }

    public void setMainText(String mainText) {
        MainText = mainText;
    }

    public int getActivityTypeID() {
        return ActivityTypeID;
    }

    public void setActivityTypeID(int activityTypeID) {
        ActivityTypeID = activityTypeID;
    }

    public int getGroupID() {
        return GroupID;
    }

    public void setGroupID(int groupID) {
        GroupID = groupID;
    }

    public int getContentID() {
        return ContentID;
    }

    public void setContentID(int contentID) {
        ContentID = contentID;
    }

    public int getLangID() {
        return LangID;
    }

    public void setLangID(int langID) {
        LangID = langID;
    }
}
