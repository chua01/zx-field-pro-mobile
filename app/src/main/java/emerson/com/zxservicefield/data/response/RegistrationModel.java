package emerson.com.zxservicefield.data.response;

/**
 * Created by mobileapps on 10/24/16.
 */

public class RegistrationModel {

    private String firstName;
    private String lasName;
    private String email;
    private String contact;
    private String announcements;
    private String busineName;
    private String city;
    private String countryCode;
    private String countryName;

    public RegistrationModel() {
    }

    public RegistrationModel(String firstName, String lasName, String email, String contact, String announcements, String busineName, String city, String countryCode, String countryName) {
        this.firstName = firstName;
        this.lasName = lasName;
        this.email = email;
        this.contact = contact;
        this.announcements = announcements;
        this.busineName = busineName;
        this.city = city;
        this.countryCode = countryCode;
        this.countryName = countryName;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLasName() {
        return lasName;
    }

    public void setLasName(String lasName) {
        this.lasName = lasName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(String announcements) {
        this.announcements = announcements;
    }

    public String getBusineName() {
        return busineName;
    }

    public void setBusineName(String busineName) {
        this.busineName = busineName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String toString() {
        return "RegistrationModel{" +
                "firstName='" + firstName + '\'' +
                ", lasName='" + lasName + '\'' +
                ", email='" + email + '\'' +
                ", contact='" + contact + '\'' +
                ", announcements='" + announcements + '\'' +
                ", busineName='" + busineName + '\'' +
                ", city='" + city + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", countryName='" + countryName + '\'' +
                '}';
    }


}
