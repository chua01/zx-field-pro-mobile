package emerson.com.zxservicefield.data;

/**
 * Created by mobileapps on 10/27/16.
 */

public class Preferences {
    public static final String SYNCONFIRSTLOGIN = "SyncInFirstLogin";

    public static final String USERNAME = "Username";
    public static final String PASSWORD = "Password";
    public static final String REMEMBER_ME = "RememberMe";
    public static final String IS_LOGGED_OUT = "IsLoggedOut";

    public static final String PROFILE = "Profile";
    public static final String PROFILE_ID = "ID";
    public static final String PROFILE_ACCOUNTID = "AccountID";
    public static final String PROFILE_CONTACTID = "ContactID";
    public static final String PROFILE_FIRSTNAME = "C_FirstName";
    public static final String PROFILE_LASTNAME = "C_LastName";
    public static final String PROFILE_PHONE = "C_Phone";
    public static final String PROFILE_COUNTRY = "A_Country";
    public static final String PROFILE_EMAIL = "U_EmailAddress";
    public static final String PROFILE_BUSINESS_NAME = "A_Name";
    public static final String PROFILE_BUSINESS_ADDRESS = "A_Address";
    public static final String PROFILE_PICTUREPATH = "ProfilePiturePath";
    public static final String PROFILE_ROLE = "C_Role";
    //Profile Questions
    public static final String PROFILE_CITY = "A_City";
    public static final String PROFILE_JOB_TITLE = "JobTitle";
    public static final String PROFILE_SELECTION_1 = "Selection1";
    public static final String PROFILE_SELECTION_2 = "Selection2";
    public static final String PROFILE_SELECTION_3 = "Selection3";
    public static final String PROFILE_SELECTION_4 = "Selection4";
    public static final String PROFILE_SELECTION_5 = "Selection5";
    public static final String PROFILE_SELECTION_6 = "Selection6";
    public static final String PROFILE_SELECTION_7 = "Selection7";
    public static final String PROFILE_SELECTION_8 = "Selection8";
    public static final String PROFILE_SELECTION_9 = "Selection9";
    public static final String PROFILE_SELECTION_10 = "Selection10";
    public static final String PROFILE_SELECTION_11 = "Selection11";

    //SYNC DATE
    public static final String MESSAGES_1_SYNDATE = "WhatsNewSyncDate";
    public static final String MESSAGES_2_SYNDATE = "TechTipsSyncDate";
    public static final String MESSAGES_3_SYNDATE = "EventsSyncDate";
    public static final String MESSAGES_4_SYNDATE = "TrainingsSyncDate";

    //PROFILE REACHED
    public static final String PROFILE_REACHED = "ProfileReached";
}
