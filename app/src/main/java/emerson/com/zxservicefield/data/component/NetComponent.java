package emerson.com.zxservicefield.data.component;

import javax.inject.Singleton;

import dagger.Component;
import emerson.com.zxservicefield.AppModule;
import emerson.com.zxservicefield.data.module.NetModule;
import retrofit2.Retrofit;

/**
 * Created by mobileapps on 10/24/16.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    // downstream components need these exposed with the return type
    // method name does not really matter
    Retrofit retrofit();
}
