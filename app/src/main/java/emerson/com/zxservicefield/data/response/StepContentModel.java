package emerson.com.zxservicefield.data.response;

/**
 * Created by mobileapps on 11/14/16.
 */

public class StepContentModel {

    private int ContentID;
    private int StepID;
    private String ImageFile;
    private String VideoFile;
    private String DateCreated;
    private int LangID;
    private String PartsTableID;
    private String ReplacementPart;

    public int getContentID() {
        return ContentID;
    }

    public void setContentID(int contentID) {
        ContentID = contentID;
    }

    public int getStepID() {
        return StepID;
    }

    public void setStepID(int stepID) {
        StepID = stepID;
    }

    public String getImageFile() {
        return ImageFile;
    }

    public void setImageFile(String imageFile) {
        ImageFile = imageFile;
    }

    public String getVideoFile() {
        return VideoFile;
    }

    public void setVideoFile(String videoFile) {
        VideoFile = videoFile;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public int getLangID() {
        return LangID;
    }

    public void setLangID(int langID) {
        LangID = langID;
    }

    public String getPartsTableID() {
        return PartsTableID;
    }

    public void setPartsTableID(String partsTableID) {
        PartsTableID = partsTableID;
    }

    public String getReplacementPart() {
        return ReplacementPart;
    }

    public void setReplacementPart(String replacementPart) {
        ReplacementPart = replacementPart;
    }

}
