package emerson.com.zxservicefield.data.response;

import java.util.ArrayList;

/**
 * Created by jeanang on 4/21/16.
 */
public class ProfileModel {
    private String ID;
    private String ContactId;
    private String ContactAccountId;
    private String C_FirstName;
    private String C_LastName;
    private String C_Role;
    private String C_Phone;
    private String A_Country;
    private String U_EmailAddress;
    private String A_Name;
    private String A_Address;
    private String A_City;
    private String JobTitle;
    private String AccountType;
    private String Selection1;
    private String Selection2;
    private ArrayList<String> Selection3 = new ArrayList<>();
    private String Selection4;
    private String Selection5;
    private ArrayList<String> Selection6 = new ArrayList<>();
    private String Selection7;
    private String Selection8;
    private String Selection9;
    private String Selection10;
    private ArrayList<String> Selection11 = new ArrayList<>();


    public String getAccountType() {
        return AccountType;
    }

    public void setAccountType(String accountType) {
        AccountType = accountType;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    public String getContactId() {
        return ContactId;
    }

    public void setContactId(String contactId) {
        ContactId = contactId;
    }

    public String getContactAccountId() {
        return ContactAccountId;
    }

    public void setContactAccountId(String contactAccountId) {
        ContactAccountId = contactAccountId;
    }


    public String getC_Role() {
        return C_Role;
    }

    public void setC_Role(String c_Role) {
        C_Role = c_Role;
    }
    public String getC_FirstName() {
        return C_FirstName;
    }

    public void setC_FirstName(String c_FirstName) {
        C_FirstName = c_FirstName;
    }

    public String getC_LastName() {
        return C_LastName;
    }

    public void setC_LastName(String c_LastName) {
        C_LastName = c_LastName;
    }

    public String getC_Phone() {
        return C_Phone;
    }

    public void setC_Phone(String c_Phone) {
        C_Phone = c_Phone;
    }

    public String getA_Country() {
        return A_Country;
    }

    public void setA_Country(String a_Country) {
        A_Country = a_Country;
    }

    public String getU_EmailAddress() {
        return U_EmailAddress;
    }

    public void setU_EmailAddress(String u_EmailAddress) {
        U_EmailAddress = u_EmailAddress;
    }

    public String getA_Name() {
        return A_Name;
    }

    public void setA_Name(String a_Name) {
        A_Name = a_Name;
    }

    public String getA_Address() {
        return A_Address;
    }

    public void setA_Address(String a_Address) {
        A_Address = a_Address;
    }

    public String getA_City() {
        return A_City;
    }

    public void setA_City(String a_City) {
        A_City = a_City;
    }

    public String getJobTitle() {
        return JobTitle;
    }

    public void setJobTitle(String jobTitle) {
        JobTitle = jobTitle;
    }

    public String getSelection1() {
        return Selection1;
    }

    public void setSelection1(String selection1) {
        Selection1 = selection1;
    }

    public String getSelection2() {
        return Selection2;
    }

    public void setSelection2(String selection2) {
        Selection2 = selection2;
    }

    public ArrayList<String> getSelection3() {
        return Selection3;
    }

    public void setSelection3(ArrayList<String> selection3) {
        Selection3 = selection3;
    }

    public String getSelection4() {
        return Selection4;
    }

    public void setSelection4(String selection4) {
        Selection4 = selection4;
    }

    public String getSelection5() {
        return Selection5;
    }

    public void setSelection5(String selection5) {
        Selection5 = selection5;
    }

    public ArrayList<String> getSelection6() {
        return Selection6;
    }

    public void setSelection6(ArrayList<String> selection6) {
        Selection6 = selection6;
    }

    public String getSelection7() {
        return Selection7;
    }

    public void setSelection7(String selection7) {
        Selection7 = selection7;
    }

    public String getSelection8() {
        return Selection8;
    }

    public void setSelection8(String selection8) {
        Selection8 = selection8;
    }

    public String getSelection9() {
        return Selection9;
    }

    public void setSelection9(String selection9) {
        Selection9 = selection9;
    }

    public String getSelection10() {
        return Selection10;
    }

    public void setSelection10(String selection10) {
        Selection10 = selection10;
    }

    public ArrayList<String> getSelection11() {
        return Selection11;
    }

    public void setSelection11(ArrayList<String> selection11) {
        Selection11 = selection11;
    }

    @Override
    public String toString() {
        return "ProfileModel{" +
                "ID='" + ID + '\'' +
                "ContactId='" + ContactId + '\'' +
                "ContactAccountId='" + ContactAccountId + '\'' +
                ", C_Role='" + C_Role + '\'' +
                ", C_FirstName='" + C_FirstName + '\'' +
                ", C_LastName='" + C_LastName + '\'' +
                ", C_Phone='" + C_Phone + '\'' +
                ", A_Country='" + A_Country + '\'' +
                ", U_EmailAddress='" + U_EmailAddress + '\'' +
                ", A_Name='" + A_Name + '\'' +
                ", A_Address='" + A_Address + '\'' +
                ", A_City='" + A_City + '\'' +
                ", JobTitle='" + JobTitle + '\'' +
                ", Selection1='" + Selection1 + '\'' +
                ", Selection2='" + Selection2 + '\'' +
                ", Selection3=" + Selection3 +
                ", Selection4='" + Selection4 + '\'' +
                ", Selection5='" + Selection5 + '\'' +
                ", Selection6=" + Selection6 +
                ", Selection7='" + Selection7 + '\'' +
                ", Selection8='" + Selection8 + '\'' +
                ", Selection9='" + Selection9 + '\'' +
                ", Selection10='" + Selection10 + '\'' +
                ", Selection11=" + Selection11 +
                '}';
    }

    public String getStringSelection3(){

        return convertArrayToStringSeperatedByComma(getSelection3());
    }

    public String getStringSelection6(){

        return convertArrayToStringSeperatedByComma(getSelection6());
    }

    public String getStringSelection11(){

        return convertArrayToStringSeperatedByComma(getSelection11());
    }

    private String convertArrayToStringSeperatedByComma(ArrayList<String> array){

        String str = "";
        for (int i = 0; i < array.size(); i++) {

            if (i == 0){
                str = array.get(i);
            }
            else {
                str = str + ", " + array.get(i);
            }

        }

        return str;
    }

    public String[] getPrimaryJobFocus() {
        return new String[]{"Air Conditioning"
                            ,"Refrigeration"
                            ,"Other"};
    }

    public String[] getContactType() {
        return new String[]{"Contractor"
                , "End User"
                , "Original Equipment Manufacturer (OEM)"
                , "Wholesaler (Emerson Authorized)"
                , "Dealer (Non Authorized Distributor)"
                , "Consultant"
                , "Education"};
    }

    public String[] getCustomerIndustry() {
        return new String[]{"Supermarket / Hypermarket"
                , "Small Food Market / Convenience Stores"
                , "Restaurants / Quick Serve (Fast Food)"
                , "Cold Rooms / Cold Storage (Small - Medium)"
                , "Blast Freezing or Food Processing"
                , "Logistics / Distribution (Large Cold Storage)"
                , "Hotels and Resorts"
                , "Residential"
                , "Commercial Retail & Office Buildings"
                , "Hospital / Medical Facility"
                , "Other"};
    }

    public String[] getJobFunction() {
        return new String[] {"Education & Training"
                , "Energy Management"
                , "Engineering"
                , "HACCP"
                , "Industry / Trade Associations"
                , "Operations / Management"
                , "Projects"
                , "Service / Repair / Maintenance"
                , "Sales & Marketing"
                , "Technical Services"};
    }

    public String[] getEducationAndTrainingRole() {
        return new String[] {"Faculty"
                , "Instructor"
                , "Student"};
    }

    public String[] getEnergyManagementRole() {
        return new String[] {"Consultant / Advisor"
                , "Audit / Control / Monitoring" };
    }

    public String[] getEngineeringRoles() {
        return new String[] {"Applications"
                , "Civil / Construction"
                , "Consultant / Advisor"
                , "Controls / Electronics"
                , "Design / Product Engineering"
                , "Drafting / CAD"
                , "Electrical/Mechanical Engineering"
                , "Executive Management"
                , "Manufacturing / Production"
                , "R&D / Testing / Laboratory"
                , "Service / Repair / Maintenance"
                , "Staff Engineer"
                , "Supervisor / Manager"};
    }

    public String[] getHACCPRole() {
        return new String[] {"Food Safety / Hygiene"
                            , "Cold Chain Control Processes" };
    }

    public String[] getIndustryTradeAssociationRoles() {
        return new String[] {"Government"
                            , "Private Organization" };
    }

    public String[] getOperationAndManagementRole() {
        return new String[] {"Accounting / Finance"
                            , "Administration & Assistance"
                            , "Engineering"
                            , "Executive Management"
                            , "Facilities Service / Maintenance"
                            , "IT/ Programming / Software"
                            , "Logistics/Warehousing/Shipping"
                            , "Owners / Partners"
                            , "Planning / Materials"
                            , "Production"
                            , "Purchasing"
                            , "Quality Control"
                            , "Supervisor / Manager"};
    }

    public String[] getProjectRole() {
        return new String[] {"Construction Management"
                , "Project Engineering"
                , "Installation / Commissioning"
                , "Service / Repair / Maintenance"
                , "Site Management / Supervisor"
                , "Support & Planning"
                , "Technician"};
    }

    public String[] getServiceRepairMaintenanceRoles() {
        return new String[] {"Engineering Support"
                            , "Supervisor / Manager"
                            , "Technician"};
    }

    public String[] getSalesAndMarketingRole() {
        return new String[] {"Administration & Assistance"
                            , "After Sales Support & Service"
                            , "Bus Dev & Key Acct Mgr"
                            , "Executive Management"
                            , "Marketing"
                            , "Sales"
                            , "Sales Design / Engineering"
                            , "Supervisor / Manager"};
    }

    public String[] getTechnicalServicesRole() {
        return new String[] {"Engineering Support"
                            , "Installation / Commissioning"
                            , "Supervisor / Manager"
                            , "Technician"};
    }

    public String[] getCompanyService() {
        return new String[] {"Design"
                            , "Equipment / Fabrication"
                            , "Installation"
                            , "Project Management"
                            , "Service & Maintenance"
                            , "Other"};
    }

    public String[] getYearsOfXPInHVACR() {
        return new String[] {"0-5 Years"
                            , "6-10 Years"
                            , "10 Years or More"};
    }

    public String[] getPreferredLanguage() {
        return new String[] {"English"
                            , "Local"};
    }

    public String[] getUserMobileOS() {
        return new String[] {"Android"
                            , "Apple (iOS)"
                            , "Others"};
    }

    public String[] getUserHasMobileConnection() {
        return new String[] {"Yes"
                            , "No"};
    }

    public String[] getKeyAreas() {
        return new String[] {"Basic Skill Improvements"
                , "Installation & Commissioning (Start-Up)"
                , "Rebuilding / Overhauling Compressors"
                , "Troubleshooting System Issues"
                , "Service / Maintenance Methods"
                , "Product Certification Programs"
                , "Electronic Controls & Monitoring"
                , "Energy Optimization Schemes"
                , "Other Skill"
                , "Other Area"};
    }
}
