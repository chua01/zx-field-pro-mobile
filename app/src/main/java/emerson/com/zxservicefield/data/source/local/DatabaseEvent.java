package emerson.com.zxservicefield.data.source.local;

/**
 * Created by mobileapps on 11/14/16.
 */

public class DatabaseEvent {
    Boolean dbCreated = false;

    public DatabaseEvent(Boolean dbCreated) {
        this.dbCreated = dbCreated;
    }

    public Boolean getDbCreated() {
        return dbCreated;
    }
}

