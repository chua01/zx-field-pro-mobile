package emerson.com.zxservicefield.data.response;

/**
 * Created by mobileapps on 10/24/16.
 */

public class ResponseObject {
    Object ResponseItem;
    String ErrorMessage;

    public ResponseObject() {
    }

    public ResponseObject(Object responseItem, String errorMessage) {
        ResponseItem = responseItem;
        ErrorMessage = errorMessage;
    }

    public Object getResponseItem() {
        return ResponseItem;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }
}

