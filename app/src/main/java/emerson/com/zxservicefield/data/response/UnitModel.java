package emerson.com.zxservicefield.data.response;

/**
 * Created by mobileapps on 11/7/16.
 */

public class UnitModel {

    String ID;
    String CustomerName;
    String ModelNumber;
    String SerialNumber;
    String EndUser;
    String ApplicationTypeID;
    String InstallationCompany;
    double LocationLat;
    double LocationLong;
    String Location;
    String MembershipID;
    String Country;
    String MemberName;
    String DateRegistered;
    String Status;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getModelNumber() {
        return ModelNumber;
    }

    public void setModelNumber(String modelNumber) {
        ModelNumber = modelNumber;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getEndUser() {
        return EndUser;
    }

    public void setEndUser(String endUser) {
        EndUser = endUser;
    }

    public String getApplicationTypeID() {
        return ApplicationTypeID;
    }

    public void setApplicationTypeID(String applicationTypeID) {
        ApplicationTypeID = applicationTypeID;
    }

    public String getInstallationCompany() {
        return InstallationCompany;
    }

    public void setInstallationCompany(String installationCompany) {
        InstallationCompany = installationCompany;
    }

    public double getLocationLat() {
        return LocationLat;
    }

    public void setLocationLat(double locationLat) {
        LocationLat = locationLat;
    }

    public double getLocationLong() {
        return LocationLong;
    }

    public void setLocationLong(double locationLong) {
        LocationLong = locationLong;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getMembershipID() {
        return MembershipID;
    }

    public void setMembershipID(String membershipID) {
        MembershipID = membershipID;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getDateRegistered() {
        return DateRegistered;
    }

    public void setDateRegistered(String dateRegistered) {
        DateRegistered = dateRegistered;
    }
}
