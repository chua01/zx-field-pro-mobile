package emerson.com.zxservicefield.data.response;

/**
 * Created by mobileapps on 11/14/16.
 */

public class StepFieldModel {

    private int ID;
    private int STEP_ID;
    private int STEPFIELD_ID;
    private int NEXTSTEP_ID;
    private String STEPFIELD_VALUE;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getSTEP_ID() {
        return STEP_ID;
    }

    public void setSTEP_ID(int STEP_ID) {
        this.STEP_ID = STEP_ID;
    }

    public int getSTEPFIELD_ID() {
        return STEPFIELD_ID;
    }

    public void setSTEPFIELD_ID(int STEPFIELD_ID) {
        this.STEPFIELD_ID = STEPFIELD_ID;
    }

    public int getNEXTSTEP_ID() {
        return NEXTSTEP_ID;
    }

    public void setNEXTSTEP_ID(int NEXTSTEP_ID) {
        this.NEXTSTEP_ID = NEXTSTEP_ID;
    }

    public String getSTEPFIELD_VALUE() {
        return STEPFIELD_VALUE;
    }

    public void setSTEPFIELD_VALUE(String STEPFIELD_VALUE) {
        this.STEPFIELD_VALUE = STEPFIELD_VALUE;
    }
}
