package emerson.com.zxservicefield.data.response;


/**
 * Created by mobileapps on 8/4/16.
 */
public class ProfileCRMModel {
    private String ContactMembershipId;
    private String ContactId;
    private String BusinessName;
    private String ContactEmailAddress;
    private String ContactLastName;
    private String ContactFirstName;
    private String ContactCellularPhone;
    private String ContactCountry;
    private String ContactAccountId;
    private String ContactAccountLocation;
    private String ContactPrimaryJobFocus;
    private String ContactType;
    private String ContactListOfJobSiteType;
    private String ContactListOfCustomerIndustry;
    private String JobFunction;
    private String JobSpecificRole;
    private String ContactListOfCompanyService;
    private String ContactYearsOfHVACExp;
    private String ContactLanguage;
    private String ContactMobileDevice;
    private String ContactHasMobileAccessOutsideOffice;
    private String ContactListOfKeyAreasToImprove;
    private String AccountType;

    public String getAccountType() {
        return AccountType;
    }

    public void setAccountType(String accountType) {
        AccountType = accountType;
    }

    public String getContactJobTitle() {
        return ContactJobTitle;
    }

    public void setContactJobTitle(String contactJobTitle) {
        ContactJobTitle = contactJobTitle;
    }

    private String ContactJobTitle;

    public String getContactCity() {
        return ContactCity;
    }

    public void setContactCity(String contactCity) {
        ContactCity = contactCity;
    }

    public String getContactAddress1() {
        return ContactAddress1;
    }

    public void setContactAddress1(String contactAddress1) {
        ContactAddress1 = contactAddress1;
    }

    private String ContactCity;
    private String ContactAddress1;

    public String getContactMembershipId() {
        return ContactMembershipId;
    }

    public void setContactMembershipId(String contactMembershipId) {
        ContactMembershipId = contactMembershipId;
    }

    public String getContactId() {
        return ContactId;
    }

    public void setContactId(String contactId) {
        ContactId = contactId;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getContactEmailAddress() {
        return ContactEmailAddress;
    }

    public void setContactEmailAddress(String contactEmailAddress) {
        ContactEmailAddress = contactEmailAddress;
    }

    public String getContactLastName() {
        return ContactLastName;
    }

    public void setContactLastName(String contactLastName) {
        ContactLastName = contactLastName;
    }

    public String getContactFirstName() {
        return ContactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        ContactFirstName = contactFirstName;
    }

    public String getContactCellularPhone() {
        return ContactCellularPhone;
    }

    public void setContactCellularPhone(String contactCellularPhone) {
        ContactCellularPhone = contactCellularPhone;
    }

    public String getContactCountry() {
        return ContactCountry;
    }

    public void setContactCountry(String contactCountry) {
        ContactCountry = contactCountry;
    }

    public String getContactAccountId() {
        return ContactAccountId;
    }

    public void setContactAccountId(String contactAccountId) {
        ContactAccountId = contactAccountId;
    }

    public String getContactAccountLocation() {
        return ContactAccountLocation;
    }

    public void setContactAccountLocation(String contactAccountLocation) {
        ContactAccountLocation = contactAccountLocation;
    }

    public String getContactPrimaryJobFocus() {
        return ContactPrimaryJobFocus;
    }

    public void setContactPrimaryJobFocus(String contactPrimaryJobFocus) {
        ContactPrimaryJobFocus = contactPrimaryJobFocus;
    }

    public String getContactType() {
        return ContactType;
    }

    public void setContactType(String contactType) {
        ContactType = contactType;
    }

    public String getContactListOfJobSiteType() {
        return ContactListOfJobSiteType;
    }

    public void setContactListOfJobSiteType(String contactListOfJobSiteType) {
        ContactListOfJobSiteType = contactListOfJobSiteType;
    }

    public String getContactListOfCustomerIndustry() {
        return ContactListOfCustomerIndustry;
    }

    public void setContactListOfCustomerIndustry(String contactListOfCustomerIndustry) {
        ContactListOfCustomerIndustry = contactListOfCustomerIndustry;
    }

    public String getJobFunction() {
        return JobFunction;
    }

    public void setJobFunction(String jobFunction) {
        JobFunction = jobFunction;
    }

    public String getJobSpecificRole() {
        return JobSpecificRole;
    }

    public void setJobSpecificRole(String jobSpecificRole) {
        JobSpecificRole = jobSpecificRole;
    }

    public String getContactListOfCompanyService() {
        return ContactListOfCompanyService;
    }

    public void setContactListOfCompanyService(String contactListOfCompanyService) {
        ContactListOfCompanyService = contactListOfCompanyService;
    }

    public String getContactYearsOfHVACExp() {
        return ContactYearsOfHVACExp;
    }

    public void setContactYearsOfHVACExp(String contactYearsOfHVACExp) {
        ContactYearsOfHVACExp = contactYearsOfHVACExp;
    }

    public String getContactLanguage() {
        return ContactLanguage;
    }

    public void setContactLanguage(String contactLanguage) {
        ContactLanguage = contactLanguage;
    }

    public String getContactMobileDevice() {
        return ContactMobileDevice;
    }

    public void setContactMobileDevice(String contactMobileDevice) {
        ContactMobileDevice = contactMobileDevice;
    }

    public String getContactHasMobileAccessOutsideOffice() {
        return ContactHasMobileAccessOutsideOffice;
    }

    public void setContactHasMobileAccessOutsideOffice(String contactHasMobileAccessOutsideOffice) {
        ContactHasMobileAccessOutsideOffice = contactHasMobileAccessOutsideOffice;
    }

    public String getContactListOfKeyAreasToImprove() {
        return ContactListOfKeyAreasToImprove;
    }

    public void setContactListOfKeyAreasToImprove(String contactListOfKeyAreasToImprove) {
        ContactListOfKeyAreasToImprove = contactListOfKeyAreasToImprove;
    }


}

