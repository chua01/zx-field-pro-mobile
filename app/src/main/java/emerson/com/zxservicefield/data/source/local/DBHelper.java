package emerson.com.zxservicefield.data.source.local;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import emerson.com.zxservicefield.data.response.StepContentModel;
import emerson.com.zxservicefield.data.response.StepFieldModel;
import emerson.com.zxservicefield.data.response.StepModel;

public class DBHelper extends SQLiteOpenHelper {

    private final String TAG = DBHelper.class.getSimpleName();

    final static int DATABASE_VERSION = 1;
    final static String DATABASE_NAME = "ZXFieldService.sqlite";
    final String DATABASE_PATH = Environment.getDataDirectory() + ("/data/emerson.com.zxservicefield/databases/");

    SQLiteDatabase database = null;
    private final Context myContext;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContext = context;
    }

    public void createDB(EventBus eventBus) {
        boolean dbExists = DBExists();
        if (dbExists) {
        } else {
            database = this.getWritableDatabase();

            try {
                copyDBFromResource();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            openDataBase();
            eventBus.post(new DatabaseEvent(true));
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            eventBus.post(new DatabaseEvent(false));
        }
    }

    private boolean DBExists() {
        SQLiteDatabase db = null;
        try {
            String databasePath = DATABASE_PATH + DATABASE_NAME;
            db = SQLiteDatabase.openDatabase(databasePath, null,
                    SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException e) {
            Log.e("SqlHelper", "Database not found");
        }
        if (db != null) {
        }
        return db != null ? true : false;
    }

    // makes a copy of the database and place it on the database path
    private void copyDBFromResource() throws IOException {
        InputStream inputStream = null;
        OutputStream outStream = null;

        String dbFilePath = DATABASE_PATH + DATABASE_NAME;
        try {
            inputStream = myContext.getAssets().open("sqlite/" + DATABASE_NAME);

            outStream = new FileOutputStream(dbFilePath);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }
            outStream.flush();
            outStream.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Error("Problem copying database from resource file.");
        }
    }

    public DBHelper openDataBase() throws SQLException {
        try {
            if (database != null) {
                if (database.isOpen()) {

                }
                String myPath = DATABASE_PATH + DATABASE_NAME;
                database = SQLiteDatabase.openDatabase(myPath, null,
                        SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public synchronized void close() {
        if (database != null)
            super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            copyDBFromResource();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            openDataBase();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("SqlHelper", "Upgrading database from version " + oldVersion
                + " to " + newVersion + ", which will destroy all old data");
        onCreate(db);
    }

    public StepModel getStep(int id) {
        SQLiteDatabase database = this.getReadableDatabase();
        StepModel stepModel = new StepModel();
        Cursor cursor = database.query(DBTables.STEPS, null, DBTables.ID + " = " + id + "", null, null, null, null, null);


        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                stepModel.setStepID(cursor.getInt(cursor.getColumnIndex(DBTables.ID)));
                stepModel.setMainText(cursor.getString(cursor.getColumnIndex(DBTables.STEPS_MAIN_TEXT)));
                stepModel.setStepName(cursor.getString(cursor.getColumnIndex(DBTables.STEPS_NAME)));
                stepModel.setContentID(cursor.getInt(cursor.getColumnIndex(DBTables.STEPS_CONTENT_ID)));
            }

        }
        cursor.close();
        database.close();
        return stepModel;
    }

    public ArrayList<StepContentModel> getStepContent(int id) {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<StepContentModel> stepsContents = new ArrayList<>();
        Cursor cursor = database.query(DBTables.STEPS_CONTENT, null, DBTables.STEPS_CONTENT_STEP_ID + " = " + id + "", null, null, null, null, null);


        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                }

        }

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();

                do {
                    StepContentModel stepContentModel = new StepContentModel();
                    stepContentModel.setImageFile(cursor.getString(cursor.getColumnIndex(DBTables.STEPS_CONTENT_IMAGE_FILE)));
                    stepContentModel.setVideoFile(cursor.getString(cursor.getColumnIndex(DBTables.STEPS_CONTENT_VIDEO_FILE)));
                    stepContentModel.setReplacementPart(cursor.getString(cursor.getColumnIndex(DBTables.STEPS_CONTENT_REPLACEMENT_PART)));
                    stepsContents.add(stepContentModel);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        database.close();
        return stepsContents;
    }

    public ArrayList<StepFieldModel> getStepField(int id) {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<StepFieldModel> stepFields = new ArrayList<>();
        Cursor cursor = database.query(DBTables.STEP_FIELDS, null, DBTables.STEP_FIELDS_STEP_ID + " = " + id + "", null, null, null, null, null);


        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();

                do {
                    StepFieldModel stepFieldModel = new StepFieldModel();
                    stepFieldModel.setNEXTSTEP_ID(cursor.getInt(cursor.getColumnIndex(DBTables.STEP_FIELDS_NEXT_STEP_ID)));
                    stepFieldModel.setSTEPFIELD_VALUE(cursor.getString(cursor.getColumnIndex(DBTables.STEP_FIELDS_STEPFIELD_VALUE)));
                    stepFields.add(stepFieldModel);
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        database.close();
        return stepFields;
    }


    public String getPart(String column,String modelnum) {
        SQLiteDatabase database = this.getReadableDatabase();
        String parts = "";
        Cursor cursor = database.query(DBTables.PARTS_TABLE, null, DBTables.PARTS_TABLE_MAIN_MODEL_NUMBER + " = '" + modelnum + "'", null, null, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                parts = cursor.getString(cursor.getColumnIndex(DBTables.PARTS_TABLE_MAIN_CONTROLLER_BOARD));
            }
        }
        database.close();
        return parts;
    }


}