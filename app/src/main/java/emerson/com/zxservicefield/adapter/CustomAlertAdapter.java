package emerson.com.zxservicefield.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import emerson.com.zxservicefield.R;

/**
 * Created by mobileapps on 8/8/16.
 */
public class CustomAlertAdapter extends RecyclerView.Adapter<CustomAlertAdapter.ViewHolder>{
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<String> items;

    public CustomAlertAdapter(Context context, ArrayList<String> list_unread_messages) {
        this.context = context;
        items =list_unread_messages;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_custom_alert_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.linItem.setTag(position);
        holder.tvItem.setText(items.get(position));

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (30 * scale + 0.5f);

        holder.itemDivider.setVisibility(View.VISIBLE);
        params.setMargins(0,0,0,0);
        holder.linItem.setLayoutParams(params);
        holder.linItem.requestLayout();

//        if (((int) holder.linItem.getTag()) == 11) {
//            holder.itemDivider.setVisibility(View.GONE);
//            params.setMargins(0,0,0,pixels);
//            holder.linItem.setLayoutParams(params);
//        } else if (((int) holder.linItem.getTag()) == 0){
//            params.setMargins(0, pixels, 0, 0);
//            holder.linItem.setLayoutParams(params);
//        }

        holder.linItem.requestLayout();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_item)
        TextView tvItem;
        @Bind(R.id.item_divider)
        View itemDivider;

        @Bind(R.id.lin_item)
        View linItem;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
