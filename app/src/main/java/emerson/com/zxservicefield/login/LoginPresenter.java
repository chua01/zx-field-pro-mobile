package emerson.com.zxservicefield.login;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import javax.inject.Inject;

import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.Preferences;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.data.response.ResponseObject;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mobileapps on 10/24/16.
 */

public class LoginPresenter implements LoginContract.Presenter{

    private LoginContract.View mView;
    private Retrofit retrofit;
    private ResponseObject responseObject;
    private String mError;

    @Inject
    public LoginPresenter(Retrofit retrofit, LoginContract.View mView) {
        this.retrofit = retrofit;
        this.mView = mView;
    }

    @Override
    public void authenticate(Activity activity,String email, String password) {
        if (TextUtils.isEmpty(email)) {
            mView.setUsernameError(activity.getResources().getString(R.string.error_invalid_id));
        } else if (TextUtils.isEmpty(password)) {
            mView.setPasswordError(activity.getResources().getString(R.string.error_invalid_password));
        } else {
            mView.setProgressBar(1);
            login(activity,email,password);
        }

    }

    private void login(final Activity activity, String email, String password){
        responseObject = new ResponseObject();
        Observable<ResponseObject> call = retrofit.create(LoginService.class).validateUser(email, password, "", "");
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<ResponseObject>() {
            @Override
            public void onCompleted() {
                Gson gson = new Gson();
                mError = responseObject.getErrorMessage();
                if (!TextUtils.isEmpty(mError)){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mView.showError(mError);
                        }
                    });
                }else{
                    Type jsonProfileFormat = new TypeToken<ProfileModel>() {}.getType();
                    ProfileModel profileModel = gson.fromJson(gson.toJson(responseObject.getResponseItem()), jsonProfileFormat);

                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Preferences.PROFILE_COUNTRY, profileModel.getA_Country());
                    editor.putString(Preferences.PROFILE_EMAIL, profileModel.getU_EmailAddress());
                    editor.putString(Preferences.PROFILE_FIRSTNAME, profileModel.getC_FirstName());
                    editor.putString(Preferences.PROFILE_ID, profileModel.getID());
                    editor.putString(Preferences.PROFILE_LASTNAME, profileModel.getC_LastName());
                    editor.putString(Preferences.PROFILE_PHONE, profileModel.getC_Phone());
                    editor.putString(Preferences.PROFILE_BUSINESS_ADDRESS, profileModel.getA_Address());
                    editor.putString(Preferences.PROFILE_BUSINESS_NAME, profileModel.getA_Name());
                    editor.putString(Preferences.PROFILE_CITY, profileModel.getA_City());
                    editor.putString(Preferences.PROFILE_ROLE, profileModel.getC_Role());
                    editor.commit();

                    ((App)activity.getApplication()).setProfileModel(profileModel);

                    Log.i("*** onCompleted ***", "error: " + responseObject.getErrorMessage());

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mView.onLogin();
                        }
                    });
                }



            }

            @Override
            public void onError(final Throwable e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                            mView.showError(activity.getResources().getString(R.string.error_no_internet));
                    }
                });
            }

            @Override
            public void onNext(ResponseObject result) {
                Log.i("*** onNext ***", "next");
                responseObject = result;
            }
        });
    }

    public interface LoginService {

        @GET("/staging/registration/webservices/PartnerPlusRegistration.asmx/zxlogin")
        Observable<ResponseObject> validateUser(@Query("user") String username, @Query("password") String password,@Query("deviceToken") String deviceToken, @Query("registrationID") String registrationID);


    }


}
