package emerson.com.zxservicefield.login;

import android.app.Activity;

/**
 * Created by mobileapps on 10/24/16.
 */

public interface LoginContract {

    interface View  {

        void showError(String error);

        void onLogin();

        void showRegistrationForm();

        void setUsernameError(String error);

        void setPasswordError(String error);

        void setProgressBar(int progress);

    }

    interface Presenter  {

        void authenticate(Activity activity,String email, String password);

    }

}
