package emerson.com.zxservicefield.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import emerson.com.zxservicefield.App;
import emerson.com.zxservicefield.R;
import emerson.com.zxservicefield.data.Preferences;
import emerson.com.zxservicefield.data.response.ProfileModel;
import emerson.com.zxservicefield.data.source.local.DatabaseEvent;
import emerson.com.zxservicefield.home.HomeActivity;
import emerson.com.zxservicefield.registration.RegistrationActivity;
import emerson.com.zxservicefield.util.AppUtils;

/**
 * Created by mobileapps on 10/24/16.
 */

public class LoginActivity extends Activity implements LoginContract.View {


    @Bind(R.id.tv_sign_up)
    TextView tvSignup;
    @Bind(R.id.main_layout)
    RelativeLayout mainLayout;
    @Bind(R.id.et_username)
    EditText etUsername;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.btn_login)
    Button btnLogin;
    @Bind(R.id.chk_remember_me)
    CheckBox chkRememberMe;
    @Bind(R.id.tv_forgot_password)
    TextView forgotPassword;


    @Inject
    LoginPresenter mPresenter;
    private AppUtils utils;
    private SweetAlertDialog dialog;
    private EventBus eventBus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        eventBus.register(this);
        App app = (App) getApplication();
        app.createDB(eventBus);

        utils = new AppUtils(this);
        setProgressDialog();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        DaggerLoginComponent.builder()
                .netComponent(((App) getApplicationContext()).getNetComponent())
                .loginModule(new LoginModule(this))
                .build().inject(this);


        if (!sharedPreferences.getString(Preferences.PROFILE_ID, "").equals("")){
            ProfileModel profileModel = new ProfileModel();
            profileModel.setA_Country(sharedPreferences.getString(Preferences.PROFILE_COUNTRY, ""));
            profileModel.setU_EmailAddress(sharedPreferences.getString(Preferences.PROFILE_EMAIL, ""));
            profileModel.setC_FirstName(sharedPreferences.getString(Preferences.PROFILE_FIRSTNAME, ""));
            profileModel.setID(sharedPreferences.getString(Preferences.PROFILE_ID, ""));
            profileModel.setC_LastName(sharedPreferences.getString(Preferences.PROFILE_LASTNAME, ""));
            profileModel.setC_Phone(sharedPreferences.getString(Preferences.PROFILE_PHONE, ""));
            profileModel.setA_Address(sharedPreferences.getString(Preferences.PROFILE_BUSINESS_ADDRESS, ""));
            profileModel.setA_Name(sharedPreferences.getString(Preferences.PROFILE_BUSINESS_NAME, ""));
            profileModel.setA_City(sharedPreferences.getString(Preferences.PROFILE_CITY, ""));
            profileModel.setC_Role(sharedPreferences.getString(Preferences.PROFILE_ROLE, ""));
            ((App)getApplication()).setProfileModel(profileModel);
            onLogin();
        }
    }

    @OnClick({R.id.tv_sign_up, R.id.btn_login, R.id.tv_forgot_password})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sign_up:
                showRegistrationForm();
                break;
            case R.id.btn_login:
                mPresenter.authenticate(LoginActivity.this, etUsername.getText().toString(), etPassword.getText().toString());
                break;
        }
    }

    @Override
    public void setUsernameError(String error) {
        etUsername.setError(error);
    }

    @Override
    public void setPasswordError(String error) {
        etPassword.setError(error);
    }

    @Override
    public void setProgressBar(int status) {
        if (status == 1) {
            dialog.show();
        }
    }

    private void setProgressDialog() {
        dialog = utils.getProgressDialog(getResources().getString(R.string.dialog_please_wait), getResources().getString(R.string.dialog_register_account));
    }

    @Override
    public void showError(String error) {
        if (dialog.isShowing())
            dialog.dismiss();
        setProgressDialog();
        dialog.setTitleText(getResources().getString(R.string.message))
                .setContentText(error)
                .setConfirmText(getResources().getString(R.string.dialog_ok))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmClickListener(null)
                .changeAlertType(SweetAlertDialog.ERROR_TYPE);

        dialog.show();
    }

    @Subscribe
    public void onDatabaseCreated(DatabaseEvent event) {
        Log.i("onDatabaseCreated", String.valueOf(event.getDbCreated()));
    }

    @Override
    public void onLogin() {
        if (dialog.isShowing())
            dialog.dismiss();
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showRegistrationForm() {
        if (dialog.isShowing())
            dialog.dismiss();
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        eventBus.unregister(this);
        super.onDestroy();
    }
}
