package emerson.com.zxservicefield.login;

import dagger.Module;
import dagger.Provides;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 10/24/16.
 */

@Module
public class LoginModule {
    private final LoginContract.View mView;


    public LoginModule(LoginContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    LoginContract.View providesLoginContractView() {
        return mView;
    }
}