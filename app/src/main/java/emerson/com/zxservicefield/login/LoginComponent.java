package emerson.com.zxservicefield.login;

import dagger.Component;
import emerson.com.zxservicefield.data.component.NetComponent;
import emerson.com.zxservicefield.util.CustomScope;

/**
 * Created by mobileapps on 10/24/16.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity activity);
}
